## 平台简介
* 前端地址：https://gitee.com/flycms_1/flycms-admin 
* 前端采用Vue、Element UI。
* 后端采用Spring Boot、Spring Security、Redis & Jwt。
* 搜索采用ElasticSearch。
* 权限认证使用Jwt，支持多终端认证系统。
* 支持加载动态权限菜单，多方式轻松权限控制。
* 高效率开发，使用代码生成器可以一键生成前后端代码。
* 感谢[Vue-Element-Admin](https://github.com/PanJiaChen/vue-element-admin)，[eladmin-web](https://gitee.com/elunez/eladmin-web?_from=gitee_search)，[RuoYi-Vue](https://gitee.com/y_project/RuoYi-Vue/tree/master)。

## 内置功能

1.  用户管理：用户是系统操作者，该功能主要完成系统用户配置。
2.  部门管理：配置系统组织机构（公司、部门、小组），树结构展现支持数据权限。
3.  岗位管理：配置系统用户所属担任职务。
4.  菜单管理：配置系统菜单，操作权限，按钮权限标识等。
5.  角色管理：角色菜单权限分配、设置角色按机构进行数据范围权限划分。
6.  字典管理：对系统中经常使用的一些较为固定的数据进行维护。
7.  参数管理：对系统动态配置常用参数。
8.  通知公告：系统通知公告信息发布维护。
9.  操作日志：系统正常操作日志记录和查询；系统异常信息日志记录和查询。
10. 登录日志：系统登录日志记录查询包含登录异常。
11. 在线用户：当前系统中活跃用户状态监控。
12. 定时任务：在线（添加、修改、删除)任务调度包含执行结果日志。
13. 代码生成：前后端代码的生成（java、html、xml、sql）支持CRUD下载 。
14. 系统接口：根据业务代码自动生成相关的api接口文档。
15. 服务监控：监视当前系统CPU、内存、磁盘、堆栈等相关信息。
16. 在线构建器：拖动表单元素生成相应的HTML代码。
17. 连接池监视：监视当前系统数据库连接池状态，可进行分析SQL找出系统性能瓶颈。
18. 小组管理：类似圈子功能，用户可以建立圈子，圈子管理员可以审核是否同意用户加入。
19. 标签管理：可以理解成是关键词关联的内容，也可以当成内容百科来使用。
20. 网站管理：可修改前台显示的网站标题和网址等等网站相关设置。
21. 用户关注：可以关注喜欢的内容，关注用户等等需要知道的内容。

## 发布程序
操作系统：CentOS，下安装程序流程
*** 
 - 安装数据库  
 ***
下载命令：
```bash
wget https://dev.mysql.com/get/mysql57-community-release-el7-9.noarch.rpm
```
然后进行yum源的安装：
```bash
rpm -ivh mysql57-community-release-el7-9.noarch.rpm
```
安装完成后，就可以使用yum命令安装mysql了：
```bash
yum -y install mysql-server
```
启动mysql：
```bash
systemctl start mysqld
```
具体使用安装方法去度娘

- jdk程序 
***
```bash
wget --no-check-certificate --no-cookies --header "Cookie: oraclelicense=accept-securebackup-cookie"  http://download.oracle.com/otn-pub/java/jdk/9.0.4+11/c2514751926b4512b076cc82f959763f/jdk-9.0.4_linux-x64_bin.tar.gz
```
- ElasticSearch 
***
ES下载地址：https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-6.5.3.tar.gz
表示不能使用root用户启动elasticsearch。我们需要创建一个用户来启动elasticsearch

groupadd elsearch
useradd elsearch -g elsearch -p elsearch
给elasticsearch目录授予elsearch权限

chown elsearch:elsearch -R /home/elasticsearch/

现在切换elsearch用户再次启动
su elsearch
cd /home/elasticsearch/
nohup ./bin/elasticsearch > /dev/null 2> /dev/null &

- 安装Redis 
***
下载redis：http://download.redis.io/releases/redis-4.0.2.tar.gz

- 申请ssl证书 
***

- Nginx 安装和设置 
***
下载nginx地址：https://nginx.org/download/nginx-1.19.4.tar.gz

- 导入数据库 
***

- 修改项目下数据库连接  
***
 - 查看 resource/application.yml spring：profiles：当前启用的哪个配置文件，这里生产环境启用prod的配置文件；
 - 修改 resource/application-prod.yml 下的数据库URL、数据库名、用户名、密码；
 
- 项目打包 
***
 ![](./docs/打包1.JPG)
 
 - 点击右侧边栏Maven，查看 profiles 是否是 prod，然后打开flycms/Lifecycle,点击install，主程序生成jar包，在项目target下可以看到生成的flycsm.jar包；
 - 查看 target/dist/flycms 目录下是否将依赖包和配置文件导出。
 
- 项目上传 
***
- 


- 启动程序 
***

- 前端下载、打包和上传 
***
后台前端源码地址：https://gitee.com/flycms_1/flycms-admin
后台登录用户名：admin，密码：123456
## 在线演示
### https://www.97560.com
- 开源不易，希望能多多支持，能让我持续维护下去。谢谢各位小伙伴。

![](./docs/index.png)

![](./docs/group.png)

![](./docs/group_index.png)

![](./docs/topic.png)

![](./docs/topic_add.png)

![](./docs/people.png)

![](./docs/my_index.png)

## 捐赠

![image](docs/支付宝收款码.jpg)
![image](docs/微信支付码.jpg)

**如果觉得这个项目对你有帮助，欢迎捐赠！**

## FlyCms前后端分离交流群

QQ群：  [![加入QQ群](https://img.shields.io/badge/211378508-blue.svg)](https://qm.qq.com/cgi-bin/qm/qr?k=CN4rdQ4NYIOuaYgj8R9EBgpXA1THUEcq&jump_from=webapi) 点击按钮入群。