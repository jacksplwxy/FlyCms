package com.flycms.common.utils;

import cn.hutool.dfa.SensitiveUtil;
import cn.hutool.dfa.WordTree;
import com.flycms.common.core.text.StrFormatter;
import com.flycms.common.exception.CustomException;
import net.sourceforge.pinyin4j.PinyinHelper;
import net.sourceforge.pinyin4j.format.HanyuPinyinCaseType;
import net.sourceforge.pinyin4j.format.HanyuPinyinOutputFormat;
import net.sourceforge.pinyin4j.format.HanyuPinyinToneType;
import org.apache.commons.lang3.StringUtils;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/**
 * 字符串工具类
 * 
 * @author kaifei sun
 */
public class StrUtils extends StringUtils
{
    /** 空字符串 */
    private static final String NULLSTR = "";

    /** 下划线 */
    private static final char SEPARATOR = '_';

    /**
     * 获取参数不为空值
     * 
     * @param value defaultValue 要判断的value
     * @return value 返回值
     */
    public static <T> T nvl(T value, T defaultValue)
    {
        return value != null ? value : defaultValue;
    }

    /**
     * * 判断一个Collection是否为空， 包含List，Set，Queue
     * 
     * @param coll 要判断的Collection
     * @return true：为空 false：非空
     */
    public static boolean isEmpty(Collection<?> coll)
    {
        return isNull(coll) || coll.isEmpty();
    }

    /**
     * * 判断一个Collection是否非空，包含List，Set，Queue
     * 
     * @param coll 要判断的Collection
     * @return true：非空 false：空
     */
    public static boolean isNotEmpty(Collection<?> coll)
    {
        return !isEmpty(coll);
    }

    /**
     * * 判断一个对象数组是否为空
     * 
     * @param objects 要判断的对象数组
     ** @return true：为空 false：非空
     */
    public static boolean isEmpty(Object[] objects)
    {
        return isNull(objects) || (objects.length == 0);
    }

    /**
     * * 判断一个对象数组是否非空
     * 
     * @param objects 要判断的对象数组
     * @return true：非空 false：空
     */
    public static boolean isNotEmpty(Object[] objects)
    {
        return !isEmpty(objects);
    }

    /**
     * * 判断一个Map是否为空
     * 
     * @param map 要判断的Map
     * @return true：为空 false：非空
     */
    public static boolean isEmpty(Map<?, ?> map)
    {
        return isNull(map) || map.isEmpty();
    }

    /**
     * * 判断一个Map是否为空
     * 
     * @param map 要判断的Map
     * @return true：非空 false：空
     */
    public static boolean isNotEmpty(Map<?, ?> map)
    {
        return !isEmpty(map);
    }

    /**
     * * 判断一个字符串是否为空串
     * 
     * @param str String
     * @return true：为空 false：非空
     */
    public static boolean isEmpty(String str)
    {
        return isNull(str) || NULLSTR.equals(str.trim());
    }

    /**
     * * 判断一个字符串是否为非空串
     * 
     * @param str String
     * @return true：非空串 false：空串
     */
    public static boolean isNotEmpty(String str)
    {
        return !isEmpty(str);
    }

    /**
     * * 判断一个对象是否为空
     * 
     * @param object Object
     * @return true：为空 false：非空
     */
    public static boolean isNull(Object object)
    {
        return object == null;
    }

    /**
     * * 判断一个对象是否非空
     * 
     * @param object Object
     * @return true：非空 false：空
     */
    public static boolean isNotNull(Object object)
    {
        return !isNull(object);
    }

    /**
     * * 判断一个对象是否是数组类型（Java基本型别的数组）
     * 
     * @param object 对象
     * @return true：是数组 false：不是数组
     */
    public static boolean isArray(Object object)
    {
        return isNotNull(object) && object.getClass().isArray();
    }

    /**
     * 去空格
     */
    public static String trim(String str)
    {
        return (str == null ? "" : str.trim());
    }

    /**
     * 截取字符串
     * 
     * @param str 字符串
     * @param start 开始
     * @return 结果
     */
    public static String substring(final String str, int start)
    {
        if (str == null)
        {
            return NULLSTR;
        }

        if (start < 0)
        {
            start = str.length() + start;
        }

        if (start < 0)
        {
            start = 0;
        }
        if (start > str.length())
        {
            return NULLSTR;
        }

        return str.substring(start);
    }

    /**
     * 截取字符串
     * 
     * @param str 字符串
     * @param start 开始
     * @param end 结束
     * @return 结果
     */
    public static String substring(final String str, int start, int end)
    {
        if (str == null)
        {
            return NULLSTR;
        }

        if (end < 0)
        {
            end = str.length() + end;
        }
        if (start < 0)
        {
            start = str.length() + start;
        }

        if (end > str.length())
        {
            end = str.length();
        }

        if (start > end)
        {
            return NULLSTR;
        }

        if (start < 0)
        {
            start = 0;
        }
        if (end < 0)
        {
            end = 0;
        }

        return str.substring(start, end);
    }

    /**
     * 格式化文本, {} 表示占位符<br>
     * 此方法只是简单将占位符 {} 按照顺序替换为参数<br>
     * 如果想输出 {} 使用 \\转义 { 即可，如果想输出 {} 之前的 \ 使用双转义符 \\\\ 即可<br>
     * 例：<br>
     * 通常使用：format("this is {} for {}", "a", "b") -> this is a for b<br>
     * 转义{}： format("this is \\{} for {}", "a", "b") -> this is \{} for a<br>
     * 转义\： format("this is \\\\{} for {}", "a", "b") -> this is \a for b<br>
     * 
     * @param template 文本模板，被替换的部分用 {} 表示
     * @param params 参数值
     * @return 格式化后的文本
     */
    public static String format(String template, Object... params)
    {
        if (isEmpty(params) || isEmpty(template))
        {
            return template;
        }
        return StrFormatter.format(template, params);
    }

    /**
     * 字符串转set
     * 
     * @param str 字符串
     * @param sep 分隔符
     * @return set集合
     */
    public static final Set<String> str2Set(String str, String sep)
    {
        return new HashSet<String>(str2List(str, sep, true, false));
    }

    /**
     * 字符串转list
     * 
     * @param str 字符串
     * @param sep 分隔符
     * @param filterBlank 过滤纯空白
     * @param trim 去掉首尾空白
     * @return list集合
     */
    public static final List<String> str2List(String str, String sep, boolean filterBlank, boolean trim)
    {
        List<String> list = new ArrayList<String>();
        if (StrUtils.isEmpty(str))
        {
            return list;
        }

        // 过滤空白字符串
        if (filterBlank && StrUtils.isBlank(str))
        {
            return list;
        }
        String[] split = str.split(sep);
        for (String string : split)
        {
            if (filterBlank && StrUtils.isBlank(string))
            {
                continue;
            }
            if (trim)
            {
                string = string.trim();
            }
            list.add(string);
        }

        return list;
    }

    /**
     * 下划线转驼峰命名
     */
    public static String toUnderScoreCase(String str)
    {
        if (str == null)
        {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        // 前置字符是否大写
        boolean preCharIsUpperCase = true;
        // 当前字符是否大写
        boolean curreCharIsUpperCase = true;
        // 下一字符是否大写
        boolean nexteCharIsUpperCase = true;
        for (int i = 0; i < str.length(); i++)
        {
            char c = str.charAt(i);
            if (i > 0)
            {
                preCharIsUpperCase = Character.isUpperCase(str.charAt(i - 1));
            }
            else
            {
                preCharIsUpperCase = false;
            }

            curreCharIsUpperCase = Character.isUpperCase(c);

            if (i < (str.length() - 1))
            {
                nexteCharIsUpperCase = Character.isUpperCase(str.charAt(i + 1));
            }

            if (preCharIsUpperCase && curreCharIsUpperCase && !nexteCharIsUpperCase)
            {
                sb.append(SEPARATOR);
            }
            else if ((i != 0 && !preCharIsUpperCase) && curreCharIsUpperCase)
            {
                sb.append(SEPARATOR);
            }
            sb.append(Character.toLowerCase(c));
        }

        return sb.toString();
    }

    /**
     * 是否包含字符串
     * 
     * @param str 验证字符串
     * @param strs 字符串组
     * @return 包含返回true
     */
    public static boolean inStringIgnoreCase(String str, String... strs)
    {
        if (str != null && strs != null)
        {
            for (String s : strs)
            {
                if (str.equalsIgnoreCase(trim(s)))
                {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 将下划线大写方式命名的字符串转换为驼峰式。如果转换前的下划线大写方式命名的字符串为空，则返回空字符串。 例如：HELLO_WORLD->HelloWorld
     * 
     * @param name 转换前的下划线大写方式命名的字符串
     * @return 转换后的驼峰式命名的字符串
     */
    public static String convertToCamelCase(String name)
    {
        StringBuilder result = new StringBuilder();
        // 快速检查
        if (name == null || name.isEmpty())
        {
            // 没必要转换
            return "";
        }
        else if (!name.contains("_"))
        {
            // 不含下划线，仅将首字母大写
            return name.substring(0, 1).toUpperCase() + name.substring(1);
        }
        // 用下划线将原始字符串分割
        String[] camels = name.split("_");
        for (String camel : camels)
        {
            // 跳过原始字符串中开头、结尾的下换线或双重下划线
            if (camel.isEmpty())
            {
                continue;
            }
            // 首字母大写
            result.append(camel.substring(0, 1).toUpperCase());
            result.append(camel.substring(1).toLowerCase());
        }
        return result.toString();
    }

    /**
     * 驼峰式命名法 例如：user_name->userName
     */
    public static String toCamelCase(String s)
    {
        if (s == null)
        {
            return null;
        }
        s = s.toLowerCase();
        StringBuilder sb = new StringBuilder(s.length());
        boolean upperCase = false;
        for (int i = 0; i < s.length(); i++)
        {
            char c = s.charAt(i);

            if (c == SEPARATOR)
            {
                upperCase = true;
            }
            else if (upperCase)
            {
                sb.append(Character.toUpperCase(c));
                upperCase = false;
            }
            else
            {
                sb.append(c);
            }
        }
        return sb.toString();
    }
    /**
     * 把骆驼命名法的变量，变为大写字母变小写且之前加下划线
     *
     * @param str
     * @return
     */
    public static String toUnderline(String str) {
        str = StringUtils.uncapitalize(str);
        char[] letters = str.toCharArray();
        StringBuilder sb = new StringBuilder();
        for (char letter : letters) {
            if (Character.isUpperCase(letter)) {
                sb.append("_" + letter + "");
            } else {
                sb.append(letter + "");
            }
        }
        return StringUtils.lowerCase(sb.toString());
    }

    /**
     * 正则表达式整个匹配
     *
     * @param pattern
     *        正则
     * @param mather
     *        需要匹配的内容
     * @return
     */
    public static boolean check(String pattern, String mather) {
        Pattern p = Pattern.compile(pattern);
        Matcher m = p.matcher(mather);
        return m.matches();
    }

    /**
     * 检查用户名是否符合要求
     * 由字母数字下划线组成且开头必须是字母，不能超过16位
     *
     * @param userName
     * @return
     */
    public static boolean checkUserName(String userName){
        if (StringUtils.isBlank(userName)) {
            return false;
        }
        return check("[a-zA-Z\u4E00-\u9FA5]{1}[a-zA-Z0-9\u4E00-\u9FA5_]{1,16}$",userName);
    }

    /**
     * 检查是否是手机号码
     *
     * @param PhoneNumber
     * @return
     */
    public static boolean checkPhoneNumber(String PhoneNumber){
        if (StringUtils.isBlank(PhoneNumber)) {
            return false;
        }
        return check("^((13[0-9])|(15[^4])|(166)|(18[0,1,2,3,5-9])|(17[0-8])|(147))\\d{8}$",PhoneNumber);
    }

    /**
     * 验证密码复杂度的正则
     * 中级（包含字母和数字）
     *
     * @param password 检查的密码字符串
     * @return
     */
    public static boolean checkPassword(String password){
        if (StringUtils.isBlank(password)) {
            return false;
        }
        return check("^(?![a-zA-z]+$)(?!\\d+$)(?![!@#$%^&*]+$)[a-zA-Z\\d!@#$%^&*]{6,32}$",password);
    }

    /**
     * 验证输入的邮箱格式是否符合
     * @param email
     * @return 是否合法
     */

    public static boolean checkEmail(String email) {
        boolean tag = true;
        final String pattern1 = "^([a-zA-Z0-9]+[_|_|.]?)*[a-zA-Z0-9]+@([a-zA-Z0-9]+[_|_|.]?)*[a-zA-Z0-9]+.[a-zA-Z]{2,4}$";
        final Pattern pattern = Pattern.compile(pattern1);
        final Matcher mat = pattern.matcher(email);
        if (!mat.find())
        {
            tag = false;
        }
        return tag;
    }

    public static boolean isNumeric(String str){
        String regular="[0-9]*";
        Pattern pattern = Pattern.compile(regular);
        Matcher isNum = pattern.matcher(str);
        if( !isNum.matches() ){
            return false;
        }
        return true;
    }

    /**
     *
     * @param str
     * @return
     */
    public static Boolean checkLong(String str) {
        try{
            Long.valueOf(str);
            return true;
        }catch (Exception e){
            return false;
        }
    }

    /**
     * 判断字符是否是中文
     *
     * @param c 字符
     * @return 是否是中文
     */
    public static boolean isChinese(char c) {
        Character.UnicodeBlock ub = Character.UnicodeBlock.of(c);
        if (ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS
                || ub == Character.UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS
                || ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A
                || ub == Character.UnicodeBlock.GENERAL_PUNCTUATION
                || ub == Character.UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION
                || ub == Character.UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS) {
            return true;
        }
        return false;
    }

    /**
     * 判断字符串是否是乱码
     *
     * @param strName 字符串
     * @return 是否是乱码
     */
    public static boolean isMessyCode(String strName) {
        String regular="\\s*|t*|r*|n*";
        Pattern p = Pattern.compile(regular);
        Matcher m = p.matcher(strName);
        String after = m.replaceAll("");
        String temp = after.replaceAll("\\p{P}", "");
        char[] ch = temp.trim().toCharArray();
        float chLength = ch.length;
        float count = 0;
        for (int i = 0; i < ch.length; i++) {
            char c = ch[i];
            if (!Character.isLetterOrDigit(c)) {
                if (!isChinese(c)) {
                    count = count + 1;
                }
            }
        }
        float result = count / chLength;
        if (result > 0.4) {
            return true;
        } else {
            return false;
        }

    }

    /**过滤HTML里去除img、p、span标签
     * @param str
     * @return
     * @throws PatternSyntaxException
     */
    public static String stringFilter(String str)throws PatternSyntaxException {

        String regEx = "<(img|p|span).*?>|</(img|p|span).*?>";
        Pattern p_html = Pattern.compile(regEx, Pattern.CASE_INSENSITIVE);
        Matcher m_html = p_html.matcher(str);
        str = m_html.replaceAll("");
        return str.trim(); // 返回文本字符串
    }

    /**
     * 去除空格与下划线
     *
     * @param str
     * @return
     */
    public static String replaceString(String str) {
        String destination = "";
        if (str != null) {
            Pattern p = Pattern.compile("\t|\r|\n| |　");
            Matcher m = p.matcher(str);
            destination = m.replaceAll("");
        }
        return destination;
    }

    /**
     * 清除字符串下划线
     * @param strKey
     * @return
     */
    public static String getNoUnderlineStr(String strKey){
        if(strKey.indexOf("_")!=-1){
            String[] keyArray = strKey.split("_");
            StringBuffer sb = new StringBuffer();
            boolean flag = false;
            for(String key:keyArray){
                if(flag){
                    //下划线后的首字母大写
                    sb.append(StrUtils.capitalize(key));
                }else{
                    flag=true;
                    sb.append(key);
                }
            }
            strKey = sb.toString();
        }
        return strKey;
    }

    /***
     * 获取拼音首字母
     *
     * @return
     */
    public static String pinyinInitials(String str) {
        char[] charArray = str.toCharArray();
        StringBuilder pinyin = new StringBuilder();
        try {
            HanyuPinyinOutputFormat defaultFormat = new HanyuPinyinOutputFormat();
            // 设置大小写格式
            defaultFormat.setCaseType(HanyuPinyinCaseType.UPPERCASE);
            // 设置声调格式：
            defaultFormat.setToneType(HanyuPinyinToneType.WITHOUT_TONE);
            for (int i = 0; i < charArray.length; i++) {
                //匹配中文,非中文转换会转换成null
                if (Character.toString(charArray[i]).matches("[\\u4E00-\\u9FA5]+")) {
                    String[] hanyuPinyinStringArray = PinyinHelper.toHanyuPinyinStringArray(charArray[i], defaultFormat);
                    if (hanyuPinyinStringArray != null) {
                        pinyin.append(hanyuPinyinStringArray[0].charAt(0));
                    }
                }else{
                    pinyin.append(charArray[i]);
                }
            }
        }
        catch (Exception e)
        {
            throw new CustomException("获取首字母失败");
        }
        return pinyin.substring(0, 1).toUpperCase();
    }

    /**
     * 分割并且去除空格
     *
     * @param str
     *            待分割字符串
     * @param sep
     *            分割符
     * @param sep2
     *            第二个分隔符
     * @return 如果str为空，则返回null。
     */
    public static String[] splitAndTrim(String str, String sep, String sep2) {
        if (StringUtils.isBlank(str)) {
            return null;
        }
        if (!StringUtils.isBlank(sep2)) {
            str = StringUtils.replace(str, sep2, sep);
        }
        String[] arr = StringUtils.split(str, sep);
        // trim
        for (int i = 0, len = arr.length; i < len; i++) {
            arr[i] = arr[i].trim();
        }
        return arr;
    }

    /**
     * 文本转html
     *
     * @param txt
     *            字符串
     * @return
     */
    public static String txt2htm(String txt) {
        if (StringUtils.isBlank(txt)) {
            return txt;
        }
        StringBuilder sb = new StringBuilder((int) (txt.length() * 1.2));
        char c;
        boolean doub = false;
        for (int i = 0; i < txt.length(); i++) {
            c = txt.charAt(i);
            if (c == ' ') {
                if (doub) {
                    sb.append(' ');
                    doub = false;
                } else {
                    sb.append("&nbsp;");
                    doub = true;
                }
            } else {
                doub = false;
                switch (c) {
                    case '&':
                        sb.append("&amp;");
                        break;
                    case '<':
                        sb.append("&lt;");
                        break;
                    case '>':
                        sb.append("&gt;");
                        break;
                    case '"':
                        sb.append("&quot;");
                        break;
                    case '\n':
                        sb.append("<br/>");
                        break;
                    default:
                        sb.append(c);
                        break;
                }
            }
        }
        return sb.toString();
    }

    /**
     * 把html内容转为文本
     *
     * @param html
     *            需要处理的html文本
     * @return
     */
    public static String trimHtml2Txt(String html) {
        // 去掉head
        html = html.replaceAll("\\<head>[\\s\\S]*?</head>(?i)", "");
        // 去掉注释
        html = html.replaceAll("\\<!--[\\s\\S]*?-->", "");
        html = html.replaceAll("\\<![\\s\\S]*?>", "");
        // 去掉样式
        html = html.replaceAll("\\<style[^>]*>[\\s\\S]*?</style>(?i)", "");
        // 去掉js
        html = html.replaceAll("\\<script[^>]*>[\\s\\S]*?</script>(?i)", "");
        // 去掉word标签
        html = html.replaceAll("\\<w:[^>]+>[\\s\\S]*?</w:[^>]+>(?i)", "");
        html = html.replaceAll("\\<xml>[\\s\\S]*?</xml>(?i)", "");
        html = html.replaceAll("\\<table>[\\s\\S]*?</table>(?i)", "");
        html = html.replaceAll("\\<html[^>]*>|<body[^>]*>|</html>|</body>(?i)", "");
        // 去掉换行
        html = html.replaceAll("\\\r\n|\n|\r", "");
        html = html.replaceAll("\\<br[^>]*>(?i)", "\r\n");
        html = html.replaceAll("\\</p>(?i)", "\r\n");
        html = html.replaceAll("\\<p>(?i)", "\r\n");

        html = html.replaceAll("\\</strong>(?i)", "");
        html = html.replaceAll("\\<strong>(?i)", "");
        // 图片替换特殊标记，客户端自己下载图片并替换标记
        // <img[^>]*/>
        /*
         * String regular="<(?i)img(.*?)src=\"(.*?)>"; String
         * img_pre="<(?i)img(.*?)src=\""; String img_sub="\"(.*?)>"; Pattern
         * p=Pattern.compile(regular,Pattern.CASE_INSENSITIVE); Matcher m =
         * p.matcher(html); String src = null; String newSrc=null; while
         * (m.find()) { src=m.group(); newSrc=src.replaceAll(img_pre,
         * Constants.API_PLACEHOLDER_IMAGE_BEGIN) .replaceAll(img_sub,
         * Constants.API_PLACEHOLDER_IMAGE_END).trim(); html=html.replace(src,
         * newSrc); } String
         * videoregular="<video(.*?)src=\"(.*?)\"(.*?)</video>"; String
         * video_pre="<video(.*?)src=\""; String video_sub="\"(.*?)</video>";
         * Pattern
         * videop=Pattern.compile(videoregular,Pattern.CASE_INSENSITIVE);
         * Matcher videom = videop.matcher(html); String videosrc = null; String
         * videonewSrc=null; while (videom.find()) { videosrc=videom.group();
         * videonewSrc=videosrc.replaceAll(video_pre,
         * Constants.API_PLACEHOLDER_VIDEO_BEGIN) .replaceAll(video_sub,
         * Constants.API_PLACEHOLDER_VIDEO_END).trim();
         * html=html.replace(videosrc, videonewSrc); }
         */
        // 去除分页
        html = html.replace("[NextPage][/NextPage]", "");
        html = html.replaceAll("\\<[^>]+>", "");
        // html = html.replaceAll("\\ ", " ");
        return html.trim();
    }


    /**
     * 去除html代码中含有的标签
     * @param htmlStr
     * @return
     */
    public static String delHtmlTags(String htmlStr) {
        //定义script的正则表达式，去除js可以防止注入
        String scriptRegex="<script[^>]*?>[\\s\\S]*?<\\/script>";
        //定义style的正则表达式，去除style样式，防止css代码过多时只截取到css样式代码
        String styleRegex="<style[^>]*?>[\\s\\S]*?<\\/style>";
        //定义HTML标签的正则表达式，去除标签，只提取文字内容
        String htmlRegex="<[^>]+>";
        //定义空格,回车,换行符,制表符
        String spaceRegex = "\\s*|\t|\r|\n";

        // 过滤script标签
        htmlStr = htmlStr.replaceAll(scriptRegex, "");
        // 过滤style标签
        htmlStr = htmlStr.replaceAll(styleRegex, "");
        // 过滤html标签
        //htmlStr = htmlStr.replaceAll(htmlRegex, "");
        // 过滤空格等
        htmlStr = htmlStr.replaceAll(spaceRegex, "");
        return htmlStr.trim(); // 返回文本字符串
    }

    /**
     * kindeditor 内容字符转义
     *
     * @param str
     * @return
     */
    public static String htmlspecialchars(String str) {
        str = str.replaceAll("&", "&amp;");
        str = str.replaceAll("<", "&lt;");
        str = str.replaceAll(">", "&gt;");
        str = str.replaceAll("\"", "&quot;");
        return str;
    }

    public static String htmlQuotConvert(String str) {
        str = str.replaceAll("\"", "###");
        return str;
    }

    public static String htmlQuotRestore(String str) {
        str = str.replaceAll("###", "\"");
        return str;
    }

    /**
     * 获取HTML代码里的内容
     * @param htmlStr
     * @return
     */
    public static String getTextFromHtml(String htmlStr){
        Pattern pattern=Pattern.compile("<((img|IMG)[^<]*?src=\"/default/images/facebox/[^\"]*\".*?)(/>|></img>|>)");
        Matcher matcher=pattern.matcher(htmlStr);
        StringBuffer imgStr = new StringBuffer();
        while(matcher.find()){
            matcher.appendReplacement(imgStr, "@@@" + htmlQuotConvert(matcher.group(1)) + "@@@");
        }
        matcher.appendTail(imgStr);
        String htmlRepla=htmlspecialchars(imgStr.toString());//将剩下的引号替换为\";
        Pattern p=Pattern.compile("@@@(.*?)@@@");
        Matcher m=p.matcher(htmlRepla);
        StringBuffer returnStr = new StringBuffer();
        while(m.find()){
            m.appendReplacement(returnStr, "<" + htmlQuotRestore(m.group(1)) + ">");
        }
        m.appendTail(returnStr);
        return returnStr.toString();
    }

    /**
     * 获取字符串数组，按逗号,分隔
     *
     * @Title: getStrArray
     * @param ids
     *            字符串
     * @return: String[]
     */
    public static String[] getStrArray(String ids) {
        String[] idArrays = null;
        if (StringUtils.isNotBlank(ids)) {
            idArrays = ids.split(",");
        }
        return idArrays;
    }

    /**
     * 显示不可见字符的Unicode
     *
     * @param input
     *            输入字符串
     * @return
     */
    public static String escapeUnicode(String input) {
        if (StringUtils.isBlank(input)) {
            return " ";
        }
        StringBuilder sb = new StringBuilder(input.length());
        @SuppressWarnings("resource")
        Formatter format = new Formatter(sb);
        for (char c : input.toCharArray()) {
            if (c < 128) {
                sb.append(c);
            } else {
                format.format("\\u%04x", (int) c);
            }
        }
        return sb.toString();
    }

    /**
     * 剪切文本。如果进行了剪切，则在文本后加上"..."
     *
     * @param s
     *            剪切对象。
     * @param len
     *            编码小于256的作为一个字符，大于256的作为两个字符。
     * @return
     */
    public static String textCut(String s, int len, String append) {
        if (s == null) {
            return null;
        }
        int slen = s.length();
        if (slen <= len) {
            return s;
        }
        // 最大计数（如果全是英文）
        int maxCount = len * 2;
        int count = 0;
        int i = 0;
        for (; count < maxCount && i < slen; i++) {
            if (s.codePointAt(i) < 256) {
                count++;
            } else {
                count += 2;
            }
        }
        if (i < slen) {
            if (count > maxCount) {
                i--;
            }
            if (!StringUtils.isBlank(append)) {
                int len256 = 256;
                if (s.codePointAt(i - 1) < len256) {
                    i -= 2;
                } else {
                    i--;
                }
                return s.substring(0, i) + append;
            } else {
                return s.substring(0, i);
            }
        } else {
            return s;
        }
    }

    /**
     * 生成邮箱随机验证码
     *
     * @return
     */
    public static String getUserCaptcha() {
        String[] beforeShuffle = new String[] { "2", "3", "4", "5", "6", "7",
                "8", "9", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J",
                "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V",
                "W", "X", "Y", "Z" };
        List<String> list = Arrays.asList(beforeShuffle);
        Collections.shuffle(list);
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < list.size(); i++) {
            sb.append(list.get(i));
        }
        String afterShuffle = sb.toString();
        String result = afterShuffle.substring(5, 9);
        //System.out.print(result);
        return result;
    }

    /**
     * 校验是否为手机号
     *
     * @Title: isPhone
     * @param phone
     *            手机号
     * @return: boolean
     */
    public static boolean isPhone(String phone) {
        String regex = "^((13[0-9])|(14[5,7,9])|(15([0-3]|[5-9]))|(166)|"
                + "(17[0,1,3,5,6,7,8])|(18[0-9])|(19[8|9]))\\d{8}$";
        int phoneLen = 11;
        if (phone.length() != phoneLen) {
            return false;
        } else {
            Pattern p = Pattern.compile(regex);
            Matcher m = p.matcher(phone);
            return m.matches();
        }
    }

    /**
     * 校验是否为邮箱
     *
     * @Title: isEmail
     * @param string
     *            字符串
     * @return: boolean
     */
    public static boolean isEmail(String string) {
        if (string == null) {
            return false;
        }
        String regEx1 = "^([a-z0-9A-Z]+[-|\\.]?)+[a-z0-9A-Z]@([a-z0-9A-Z]+(-[a-z0-9A-Z]+)?\\.)+[a-zA-Z]{2,}$";
        Pattern p;
        Matcher m;
        p = Pattern.compile(regEx1);
        m = p.matcher(string);
        if (m.matches()) {
            return true;
        } else {
            return false;
        }
    }

    /** * 判断是否为合法IP * @return the ip */
    public static boolean isboolIp(String ipAddress) {
        String ip = "((2[0-4]\\d|25[0-5]|[01]?\\d\\d?)\\.){3}(2[0-4]\\d|25[0-5]|[01]?\\d\\d?)";
        Pattern pattern = Pattern.compile(ip);
        Matcher matcher = pattern.matcher(ipAddress);
        return matcher.matches();
    }

    /**
     * 校验是否为为银行卡
     *
     * @Title: isBankCard
     * @param cardId
     *            号码
     * @return: boolean
     */
    public static boolean isBankCard(String cardId) {
        int cardIdLenMin = 16;
        int cardIdLenMax = 19;
        if (cardId == null || cardId.length() < cardIdLenMin || cardId.length() > cardIdLenMax) {
            return false;
        }

        char bit = getBankCardCheckCode(cardId.substring(0, cardId.length() - 1));
        char n = 'N';
        if (bit == n) {
            return false;
        }
        return cardId.charAt(cardId.length() - 1) == bit;
    }

    private static char getBankCardCheckCode(String nonCheckCodeCardId) {
        if (nonCheckCodeCardId == null || nonCheckCodeCardId.trim().length() == 0
                || !nonCheckCodeCardId.matches("\\d+")) {
            // 如果传的不是数据返回N
            return 'N';
        }
        char[] chs = nonCheckCodeCardId.trim().toCharArray();
        int luhnSum = 0;
        for (int i = chs.length - 1, j = 0; i >= 0; i--, j++) {
            int k = chs[i] - '0';
            if (j % 2 == 0) {
                k *= 2;
                k = k / 10 + k % 10;
            }
            luhnSum += k;
        }
        return (luhnSum % 10 == 0) ? '0' : (char) ((10 - luhnSum % 10) + '0');
    }

    /**
     * 隐藏关键信息数据， 手机号：保留前三位及后四位 邮箱：保留@前面一位，@后面全部显示 银行卡：保留前四位及后四位 普通用户名：保留前一位及后一位
     *
     * @Title: hideStr
     * @param str
     *            源字符串
     * @return: String
     */
    public static String hideStr(String str) {
        if (StringUtils.isBlank(str)) {
            return "";
        } else if (isPhone(str)) {
            return str.substring(0, 3) + "****" + str.substring(str.length() - 4);
        } else if (isEmail(str)) {
            int index = str.indexOf("@");
            String emailName = str.substring(0, index + 1);
            return emailName.substring(0, 1) + "****" + str.substring(index);
        } else if (isBankCard(str)) {
            return str.substring(0, 4) + "****" + str.substring(str.length() - 4);
        } else {
            return str.substring(0, 1) + "****" + str.substring(str.length() - 1);
        }
    }

    /**
     * 判断int数组中是否存在某个值的方法
     *
     * @param intArr 待查找int数组
     * @param intt 需要查找的值
     * @return :存在 true :不存在 false
     */
    public static boolean IntArrLookupInt(int[] intArr, int intt) {
        String b = intt + ""; // 先转换为String类型
        for (int i : intArr) {
            if (b.equals(i + "")) {
                return true;
            }
        }
        return false;
    }

    /**
     * Long[] 转String[]
     *
     * @param longArray
     * @return
     */
    public static String[] longToString(Long longArray[]) {
        if (longArray == null || longArray.length < 1) {
            return null;
        }
        String  stringArray[] = new String[longArray.length];
        for (int i = 0; i < stringArray.length; i++ ) {
            try {
                stringArray[i] = String.valueOf(longArray[i]);
            } catch (NumberFormatException e) {
                stringArray[i] = null;
                continue;
            }
        }
        return stringArray;
    }

    /**
     * 获取图片地址的图片后缀
     *
     * @param url
     *        网络图片地址
     * @return
     */
    public static String getImageUrlSuffix(String url){
        Pattern p =Pattern.compile("\\/[^\\/]*\\.(jpg|jpeg|gif|png|bmp)\\??[^?]*$", Pattern.CASE_INSENSITIVE);
        Matcher matcher = p.matcher(url);
        String regex ="";
        if(matcher.find()){
            regex = matcher.group(1);
        }
        return regex;
    }

    /**
     * 获取图片本站图片地址根目录路径
     *
     * @param url
     * @return
     */
    public static String getImageRootUrl(String url){
        Pattern p =Pattern.compile("^[http://|ftp://|https://|www]+[^\\/]*\\/(.*\\.(jpg|jpeg|gif|png|bmp))\\??[^?]*$", Pattern.CASE_INSENSITIVE);
        Matcher matcher = p.matcher(url);
        String regex ="";
        if(matcher.find()){
            regex = matcher.group(1);
        }
        return regex;
    }

     /**
      * 获取url的顶级域名
      * @param
      * @return
      */
     public static String getTopDomain(String url){
         String pattern = "[^*]*?\\.(com.cn|com.hk|net.cn|org.cn|gov.cn|edu.cn|com|cn|org|gov|net|edu|xyz|xin|club|shop|site|wang|top|win|online|tech|store|bid|cc|ren|lol|pro|red|kim|space|link|click|news|news|ltd|website|biz|help|mom|work|date|loan|mobi|live|studio|info|pics|photo|trade|vc|party|game|rocks|band|gift|wiki|design|software|social|lawyer|engineer|org|name|tv|me|asia|co|press|video|market|games|science|中国|公司|网络|pub|la|auction|email|sex|sexy|one|host|rent|fans|cn.com|life|cool|run|gold|rip|ceo|sale|hk|io|gg|tm|gs|us)";
         Pattern p = Pattern.compile(pattern,Pattern.CASE_INSENSITIVE);
         Matcher matcher = p.matcher(url);
         if(matcher.find()){
             return matcher.group();
         }
         return null;
     }

    // 查找评论里at的用户名
    public static List<String> fetchAtUser(String content) {
        if (org.springframework.util.StringUtils.isEmpty(content)) return Collections.emptyList();
        // 去掉 ``` ``` 包围的内容
        content = content.replaceAll("```([\\s\\S]*)```", "");
        // 去掉 ` ` 包围的内容
        content = content.replaceAll("`([\\s\\S]*)`", "");
        // 找到@的用户
        String atRegex = "@[a-z0-9-\\u4E00-\\u9FA5_]+\\b?";
        List<String> atUsers = new ArrayList<>();
        Pattern regex = Pattern.compile(atRegex);
        Matcher regexMatcher = regex.matcher(content);
        while (regexMatcher.find()) {
            atUsers.add(regexMatcher.group());
        }
        return atUsers;
    }

    public static void main(String[] args) {
        String text = "@fff @ffff @123123 @飞舞 sdfsaf@我有一werwqrwq颗大&&土豆，刚出**锅的";
        System.out.println("-------------------------------" + fetchAtUser(text));
    }
}