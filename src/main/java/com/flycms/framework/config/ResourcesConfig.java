package com.flycms.framework.config;

import com.flycms.framework.interceptor.RepeatSubmitInterceptor;
import com.flycms.common.constant.Constants;
import com.flycms.framework.config.properties.FlyMallProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.Resource;

/**
 * 通用配置
 * 
 * @author kaifei sun
 */
@Configuration
public class ResourcesConfig implements WebMvcConfigurer
{
    @Resource
    private RepeatSubmitInterceptor repeatSubmitInterceptor;

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry)
    {
        /** 本地文件上传路径 */
        String os = System.getProperty("os.name");
        //如果是Windows系统
        if (os.toLowerCase().startsWith("win")) {
            registry.addResourceHandler(Constants.RESOURCE_PREFIX + "/**")
                    // /uploads/**表示在磁盘filePathWindow目录下的所有资源会被解析为以下的路径
                    .addResourceLocations("file:" + FlyMallProperties.getFilePathWindow()+ "/");
        } else {  //linux 和mac
            registry.addResourceHandler(Constants.RESOURCE_PREFIX + "/**")
                    .addResourceLocations("file:" + FlyMallProperties.getFilePathLinux()+ "/") ;
        }

        registry.addResourceHandler("/**")
                .addResourceLocations("classpath:/resources/",
                "file:./views/static/");
        /** swagger配置 */
        registry.addResourceHandler("swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");
        registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
    }

    /**
     * 自定义拦截规则
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry)
    {
        registry.addInterceptor(repeatSubmitInterceptor).addPathPatterns("/**").excludePathPatterns("/js/sea.config.js");
    }

}