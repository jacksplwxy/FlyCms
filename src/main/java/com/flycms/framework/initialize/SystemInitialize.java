package com.flycms.framework.initialize;

import com.flycms.common.utils.spring.SpringContextUtil;
import com.flycms.modules.site.domain.Site;
import com.flycms.modules.site.service.ISiteService;
import freemarker.template.Configuration;
import freemarker.template.TemplateModelException;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

/**
 * Biz-Boot, All rights reserved
 * 版权：企业之家网 -- 企业建站管理系统<br/>
 * 开发公司：97560.com<br/>
 *
 * 程序部分参数初始化服务启动
 *
 * @author sun-kaifei
 * @version 1.0 <br/>
 * @email 79678111@qq.com
 * @Date: 13:05 2019/8/17
 */
@Service
public class SystemInitialize {

    @Resource
    private Configuration configuration;
    @PostConstruct
    public void setSharedVariable() throws TemplateModelException {
        ISiteService siteService = SpringContextUtil.getBean(ISiteService.class);
        Site site=siteService.selectSite(478279584488632368l);
        //网站名称
        configuration.setSharedVariable("site", site);
    }
}
