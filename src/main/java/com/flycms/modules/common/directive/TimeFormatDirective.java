package com.flycms.modules.common.directive;

import com.flycms.common.utils.DateUtils;
import com.flycms.common.utils.StrUtils;
import com.flycms.common.web.freemarker.DirectiveUtils;
import freemarker.core.Environment;
import freemarker.template.TemplateDirectiveBody;
import freemarker.template.TemplateDirectiveModel;
import freemarker.template.TemplateException;
import freemarker.template.TemplateModel;
import lombok.SneakyThrows;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

/**
 * 文本字符串截断
 * @author: kaifei sun
 * @date:   2020年11月17日 下午4:50:51
 */
@Component
public class TimeFormatDirective implements TemplateDirectiveModel {

	public static final String PARAM_S = "time";

	@SneakyThrows
	@SuppressWarnings("unchecked")
	public void execute(Environment env, Map params, TemplateModel[] loopVars,
						TemplateDirectiveBody body) throws TemplateException, IOException {
		String time = DirectiveUtils.getString(PARAM_S, params);
		//String format = DirectiveUtils.getString("format", params);
		if (time != null) {
			//System.out.println("++++++++++++++:"+time);
			Writer out = env.getOut();
			out.append(DateUtils.getDateTimeString(time));
/*			if(DateUtils.isTimeStampValid(time)){
				Writer out = env.getOut();

				if("1".equals(format)){
					//将字符串日期转换为字符串 , 格式yyyy-MM-dd HH:mm:ss
					out.append(DateUtils.fomatString(new Date(Long.valueOf(time)).toString()));
				}else if("2".equals(format)){
					out.append(DateUtils.getDateTimeString(time));
				}else if("3".equals(format)){
					out.append(DateUtils.getMessageDateTime(time));
				}
			}*/
		}
	}
}
