package com.flycms.modules.data.controller.front;

import com.flycms.common.utils.SessionUtils;
import com.flycms.common.utils.StrUtils;
import com.flycms.framework.web.controller.BaseController;
import com.flycms.framework.web.domain.AjaxResult;
import com.flycms.modules.data.domain.Label;
import com.flycms.modules.data.domain.LabelFollow;
import com.flycms.modules.data.domain.dto.LabelDTO;
import com.flycms.modules.data.service.ILabelFollowService;
import com.flycms.modules.data.service.ILabelService;
import com.flycms.modules.user.domain.User;
import com.flycms.modules.user.domain.UserFans;
import com.flycms.modules.user.service.IUserAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@Controller
public class LabelController extends BaseController{
    @Autowired
    private ILabelService labelService;
    @Autowired
    private ILabelFollowService labelFollowService;

    @Autowired
    private IUserAccountService userAccountService;

    //登录处理
    @ResponseBody
    @PostMapping(value = "/t/user/follow")
    public AjaxResult userLogin(@RequestParam(value = "id", required = false) String id){
        if (!StrUtils.checkLong(id)) {
            return AjaxResult.error(0,"id参数不正确");
        }
        LabelDTO label = labelService.findLabelById(Long.valueOf(id));
        if(label == null){
            return AjaxResult.error(1,"您关注的标签不存在");
        }
        User login = SessionUtils.getUser();
        if(login == null){
            return AjaxResult.error(2,"请登录后在关注操作");
        }
        if(labelFollowService.checkLabelFollow(Long.valueOf(id),login.getId())){
            labelFollowService.deleteLabelFollow(Long.valueOf(id),login.getId());
            labelService.updateLabelFollowCount(Long.valueOf(id));
            userAccountService.updateUserFollowLabel(login.getId());
            Map<String, String> map = new HashMap<String, String>();
            map.put("count",String.valueOf(label.getCountFollow()-1));
            return AjaxResult.success(3,"已取消关注",map);
        }else{
            LabelFollow labelFollow=new LabelFollow();
            labelFollow.setLabelId(Long.valueOf(id));
            labelFollow.setUserId(login.getId());
            labelFollowService.insertLabelFollow(labelFollow);
            labelService.updateLabelFollowCount(Long.valueOf(id));
            userAccountService.updateUserFollowLabel(login.getId());
            Map<String, String> map = new HashMap<String, String>();
            map.put("count",String.valueOf(label.getCountFollow()+1));
            return AjaxResult.success(4,"已成功关注",map);
        }
    }

    /**
     * 首页
     *
     * @return
     */
    @GetMapping(value = {"/t/label-index","/t/label-index-p-{p}"})
    public String labelList(@PathVariable(value = "p", required = false) String p, ModelMap modelMap){
        if(p == null) p = "1";
        modelMap.addAttribute("p", p);
        return theme.getPcTemplate("label/list_label");
    }

    /**
     * 获取解梦内容详细信息
     */
    @GetMapping(value = "/t/{shortUrl}")
    public String getInfo(@PathVariable(value = "shortUrl", required = false) String shortUrl, @RequestParam(value = "p", defaultValue = "1") int p, ModelMap modelMap)
    {
        if (StrUtils.isEmpty(shortUrl)) {
            return forward("/error/404");
        }
        Label content= labelService.findLabelByShorturl(shortUrl);
        if(content == null){
            return forward("/error/404");
        }
        modelMap.addAttribute("p",p);
        modelMap.addAttribute("content",content);
        return theme.getPcTemplate("label/detail");
    }
}
