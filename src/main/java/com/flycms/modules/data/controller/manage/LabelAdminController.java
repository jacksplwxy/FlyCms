package com.flycms.modules.data.controller.manage;


import com.flycms.common.constant.UserConstants;
import com.flycms.common.utils.page.Pager;
import com.flycms.common.validator.Order;
import com.flycms.common.validator.Sort;
import com.flycms.modules.monitor.domain.FlyJob;
import org.quartz.SchedulerException;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.flycms.framework.aspectj.lang.annotation.Log;
import com.flycms.framework.aspectj.lang.enums.BusinessType;
import com.flycms.modules.data.domain.Label;
import com.flycms.modules.data.domain.dto.LabelDTO;
import com.flycms.modules.data.service.ILabelService;
import com.flycms.framework.web.controller.BaseController;
import com.flycms.framework.web.domain.AjaxResult;
import com.flycms.common.utils.poi.ExcelUtil;
import com.flycms.framework.web.page.TableDataInfo;

import java.util.List;

/**
 * 标签Controller
 * 
 * @author admin
 * @date 2020-11-18
 */
@RestController
@RequestMapping("/system/data/label")
public class LabelAdminController extends BaseController
{
    @Autowired
    private ILabelService labelService;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////

    /**
     * 新增标签
     */
    @PreAuthorize("@ss.hasPermi('data:label:add')")
    @Log(title = "标签", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody Label label)
    {
        if (UserConstants.NOT_UNIQUE.equals(labelService.checkLabelLabelNameUnique(label)))
        {
            return AjaxResult.error("新增标签'" + label.getTitle() + "'失败，标签名称已存在");
        }
        return toAjax(labelService.insertLabel(label));
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除标签
     */
    @PreAuthorize("@ss.hasPermi('data:label:remove')")
    @Log(title = "标签", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(labelService.deleteLabelByIds(ids));
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改标签
     */
    @PreAuthorize("@ss.hasPermi('data:label:edit')")
    @Log(title = "标签", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody Label label)
    {
        if (UserConstants.NOT_UNIQUE.equals(labelService.checkLabelLabelNameUnique(label)))
        {
            return AjaxResult.error("新增标签'" + label.getTitle() + "'失败，标签名称已存在");
        }
        return toAjax(labelService.updateLabel(label));
    }

    /**
     * 定时任务状态修改
     */
    @PreAuthorize("@ss.hasPermi('data:label:edit')")
    @Log(title = "标签", businessType = BusinessType.UPDATE)
    @PutMapping("/changeStatus")
    public AjaxResult changeStatus(@RequestBody Label label) throws SchedulerException
    {
        return toAjax(labelService.updateLabelStatus(label.getId(),label.getStatus()));
    }
    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 查询标签列表
     */
    @PreAuthorize("@ss.hasPermi('data:label:list')")
    @GetMapping("/list")
    public TableDataInfo list(Label label,
        @RequestParam(defaultValue = "1") Integer pageNum,
        @RequestParam(defaultValue = "10") Integer pageSize,
        @Sort @RequestParam(defaultValue = "id") String sort,
        @Order @RequestParam(defaultValue = "desc") String order)
    {
        Pager<LabelDTO> pager = labelService.selectLabelPager(label, pageNum, pageSize, sort, order);
        return getDataTable(pager.getList(),pager.getTotal());
    }

    /**
     * 导出标签列表
     */
    @PreAuthorize("@ss.hasPermi('data:label:export')")
    @Log(title = "标签", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(Label label)
    {
        List<LabelDTO> labelList = labelService.exportLabelList(label);
        ExcelUtil<LabelDTO> util = new ExcelUtil<LabelDTO>(LabelDTO.class);
        return util.exportExcel(labelList, "label");
    }

    /**
     * 获取标签详细信息
     */
    @PreAuthorize("@ss.hasPermi('data:label:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(labelService.findLabelById(id));
    }

}
