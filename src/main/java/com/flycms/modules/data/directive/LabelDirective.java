package com.flycms.modules.data.directive;

import com.flycms.framework.web.tag.BaseTag;
import com.flycms.modules.data.domain.Label;
import com.flycms.modules.data.domain.LabelFollow;
import com.flycms.modules.data.domain.LabelMerge;
import com.flycms.modules.data.domain.dto.LabelDTO;
import com.flycms.modules.data.service.ILabelFollowService;
import com.flycms.modules.data.service.ILabelMergeService;
import com.flycms.modules.data.service.ILabelService;
import com.flycms.modules.group.domain.Group;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class LabelDirective extends BaseTag {

    @Autowired
    private ILabelService labelService;;

    @Autowired
    private ILabelMergeService labelMergeService;

    @Autowired
    private ILabelFollowService labelFollowService;

    public LabelDirective() {
        super(LabelDirective.class.getName());
    }

    public Object paginate(Map params) {
        int pageNum = this.getPageNum(params);
        int pageSize = this.getPageSize(params);
        String title = getParam(params, "title");
        String sort = getParam(params, "sort");
        String order = getParam(params, "order");
        Label label = new Label();
        label.setTitle(title);
        return labelService.selectLabelPager(label, pageNum, pageSize, sort, order);
    }

    /**
     * 标签详细信息
     *
     * @param params
     * @return
     */
    public Object info(Map params) {
        Long id = getLongParam(params, "id");
        LabelDTO label = null;
        if(id != null){
            label = labelService.findLabelById(id);
        }
        return label;
    }

    /**
     * 查询文章和标签关联翻页
     *
     * @param params
     * @return
     */
    public Object infoList(Map params) {
        int pageNum = this.getPageNum(params);
        int pageSize = this.getPageSize(params);
        Long labelId = getLongParam(params, "labelId");
        Long infoId = getLongParam(params, "infoId");
        int infoType = getParamInt(params, "infoType");
        String status = getParam(params,"status");
        String sort = getParam(params, "sort");
        String order = getParam(params, "order");
        LabelMerge labelMerge = new LabelMerge();
        labelMerge.setLabelId(labelId);
        labelMerge.setInfoId(infoId);
        labelMerge.setInfoType(infoType);
        labelMerge.setStatus(status);
        return labelMergeService.selectLabelMergePager(labelMerge, pageNum, pageSize, sort, order);
    }

    /**
     * 标签详细信息
     *
     * @param params
     * @return
     */
    public Object checkFollow(Map params) {
        Long id = getLongParam(params, "id");
        Long userId = getLongParam(params, "userId");
        if(id == null && userId ==null){
            return false;
        }
        Boolean label = labelFollowService.checkLabelFollow(id,userId);
        return label;
    }

    /**
     * 查询用户和标签关联翻页
     *
     * @param params
     * @return
     */
    public Object followList(Map params) {
        int pageNum = this.getPageNum(params);
        int pageSize = this.getPageSize(params);
        Long labelId = getLongParam(params, "labelId");
        Long userId = getLongParam(params, "userId");
        String sort = getParam(params, "sort");
        String order = getParam(params, "order");
        LabelFollow labelFollow = new LabelFollow();
        labelFollow.setLabelId(labelId);
        labelFollow.setUserId(userId);
        return labelFollowService.selectLabelFollowPager(labelFollow, pageNum, pageSize, sort, order);
    }
}
