package com.flycms.modules.data.domain;

import com.flycms.framework.web.domain.BaseEntity;
import lombok.Data;

import java.util.Date;

/**
 * 标签对象 fly_label
 * 
 * @author admin
 * @date 2020-11-18
 */
@Data
public class Label extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;
    /**短域名 */
    private String shortUrl;
    /** 信息id */
    private Long infoId;
    /** 信息类别 */
    private Integer infoType;
    /** 标签名称 */
    private String title;
    /** 标签说明 */
    private String content;
    /** 标签ico */
    private String labelPic;
    /** 使用数量 */
    private Integer countTopic;
    /** 使用数量 */
    private Integer countGroup;
    //关注数
    private Integer countFollow;
    /** 话题是否锁定 1 锁定 0 未锁定 */
    private Integer labelLock;
    /** 创建时间 */
    private Date createTime;
    /** 状态 */
    private String status;
}
