package com.flycms.modules.data.mapper;

import java.util.List;
import com.flycms.common.utils.page.Pager;
import com.flycms.modules.data.domain.LabelMerge;
import org.springframework.stereotype.Repository;

/**
 * 内容与标签关联Mapper接口
 * 
 * @author admin
 * @date 2020-11-18
 */
@Repository
public interface LabelMergeMapper 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增内容与标签关联
     *
     * @param labelMerge 内容与标签关联
     * @return 结果
     */
    public int insertLabelMerge(LabelMerge labelMerge);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除内容与标签关联
     *
     * @param labelId 内容与标签关联ID
     * @return 结果
     */
    public int deleteLabelMergeById(Long labelId);

    /**
     * 批量删除内容与标签关联
     *
     * @param labelIds 需要删除的数据ID
     * @return 结果
     */
    public int deleteLabelMergeByIds(Long[] labelIds);

    /**
     * 按内容id和标签id删除内容与标签关联
     *
     * @param labelId
     * @param infoId
     * @return
     */
    public int deleteLabelMerge(Long labelId,Long infoId);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改内容与标签关联
     *
     * @param labelMerge 内容与标签关联
     * @return 结果
     */
    public int updateLabelMerge(LabelMerge labelMerge);


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////

    /**
     * 查询标签关联是否已关联
     *
     * @param labelMerge 关联实体类
     * @return
     */
    public int checkLabelMergeUnique(LabelMerge labelMerge);
    /**
     * 查询内容与标签关联
     * 
     * @param labelId 内容与标签关联ID
     * @return 内容与标签关联
     */
    public LabelMerge findLabelMergeById(Long labelId);

    /**
     * 按ID查询关联信息
     *
     * @param id
     * @return
     */
    public List<LabelMerge> gelLabelListById(Long id);

    /**
     * 按ID查询内容相关的标签
     *
     * @param id
     * @return
     */
    public List<String> queryLabelListById(Long id);

    /**
     * 查询内容与标签关联数量
     *
     * @param pager 分页处理类
     * @return 内容与标签关联数量
     */
    public int queryLabelMergeTotal(Pager pager);

    /**
     * 查询内容与标签关联列表
     * 
     * @param pager 分页处理类
     * @return 内容与标签关联集合
     */
    public List<LabelMerge> selectLabelMergePager(Pager pager);

    /**
     * 查询需要导出的内容与标签关联列表
     *
     * @param labelMerge 内容与标签关联
     * @return 内容与标签关联集合
     */
    public List<LabelMerge> exportLabelMergeList(LabelMerge labelMerge);
}
