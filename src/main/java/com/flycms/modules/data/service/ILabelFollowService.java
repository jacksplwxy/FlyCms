package com.flycms.modules.data.service;

import com.flycms.common.utils.page.Pager;
import com.flycms.modules.data.domain.LabelFollow;
import com.flycms.modules.data.domain.dto.LabelFollowDTO;

import java.util.List;

/**
 * 话题关注Service接口
 * 
 * @author admin
 * @date 2021-02-01
 */
public interface ILabelFollowService 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增话题关注
     *
     * @param labelFollow 话题关注
     * @return 结果
     */
    public int insertLabelFollow(LabelFollow labelFollow);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////

    /**
     * 删除话题关注信息
     *
     * @param id 话题关注ID
     * @return 结果
     */
    public int deleteLabelFollowById(Long id);

    /**
     * 批量删除话题关注
     *
     * @param ids 需要删除的话题关注ID
     * @return 结果
     */
    public int deleteLabelFollowByIds(Long[] ids);

    /**
     * 删除用户和标签关联
     *
     * @param labelId 标签id
     * @param userId 用户id
     * @return 结果
     */
    public int deleteLabelFollow(Long labelId,Long userId);
    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改话题关注
     *
     * @param labelFollow 话题关注
     * @return 结果
     */
    public int updateLabelFollow(LabelFollow labelFollow);

    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     *
     * 检查是否已关注该标签
     *
     * @param labelId 标签id
     * @param userId 用户id
     * @return
     */
    public boolean checkLabelFollow(Long labelId,Long userId);

    /**
     * 查询话题关注
     * 
     * @param id 话题关注ID
     * @return 话题关注
     */
    public LabelFollowDTO findLabelFollowById(Long id);

    /**
     * 查询话题关注列表
     * 
     * @param labelFollow 话题关注
     * @return 话题关注集合
     */
    public Pager<LabelFollowDTO> selectLabelFollowPager(LabelFollow labelFollow, Integer page, Integer limit, String sort, String order);

    /**
     * 查询需要导出的话题关注列表
     *
     * @param labelFollow 话题关注
     * @return 话题关注集合
     */
    public List<LabelFollowDTO> exportLabelFollowList(LabelFollow labelFollow);
}
