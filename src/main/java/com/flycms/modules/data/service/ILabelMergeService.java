package com.flycms.modules.data.service;

import com.flycms.common.utils.page.Pager;
import com.flycms.modules.data.domain.LabelMerge;
import com.flycms.modules.data.domain.dto.LabelMergeDTO;

import java.util.List;

/**
 * 内容与标签关联Service接口
 * 
 * @author admin
 * @date 2020-11-18
 */
public interface ILabelMergeService 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增内容与标签关联
     *
     * @param labelMerge 内容与标签关联
     * @return 结果
     */
    public int insertLabelMerge(LabelMerge labelMerge);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除内容与标签关联
     *
     * @param labelIds 需要删除的内容与标签关联ID
     * @return 结果
     */
    public int deleteLabelMergeByIds(Long[] labelIds);

    /**
     * 删除内容与标签关联信息
     *
     * @param labelId 内容与标签关联ID
     * @return 结果
     */
    public int deleteLabelMergeById(Long labelId);

    /**
     * 按内容id和标签id删除内容与标签关联
     *
     * @param labelId
     * @param infoId
     * @return
     */
    public int deleteLabelMerge(Long labelId,Long infoId);
    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改内容与标签关联
     *
     * @param labelMerge 内容与标签关联
     * @return 结果
     */
    public int updateLabelMerge(LabelMerge labelMerge);

    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 查询标签关联是否已关联
     *
     * @param labelMerge 关联实体类
     * @return
     */
    public boolean checkLabelMergeUnique(LabelMerge labelMerge);

    /**
     * 查询内容与标签关联
     * 
     * @param labelId 内容与标签关联ID
     * @return 内容与标签关联
     */
    public LabelMergeDTO findLabelMergeById(Long labelId);

    /**
     * 按ID查询关联信息
     *
     * @param id 标签id
     * @return
     */
    public List<LabelMerge> gelLabelListById(Long id);

    /**
     * 按ID查询内容相关的标签
     *
     * @param id 标签id
     * @return
     */
    public List<String> queryLabelListById(Long id);

    /**
     * 查询内容与标签关联列表
     * 
     * @param labelMerge 内容与标签关联
     * @return 内容与标签关联集合
     */
    public Pager<LabelMergeDTO> selectLabelMergePager(LabelMerge labelMerge, Integer page, Integer limit, String sort, String order);

    /**
     * 查询需要导出的内容与标签关联列表
     *
     * @param labelMerge 内容与标签关联
     * @return 内容与标签关联集合
     */
    public List<LabelMergeDTO> exportLabelMergeList(LabelMerge labelMerge);
}
