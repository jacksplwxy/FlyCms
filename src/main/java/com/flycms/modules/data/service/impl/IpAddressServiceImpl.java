package com.flycms.modules.data.service.impl;


import com.flycms.common.utils.bean.BeanConvertor;
import com.flycms.common.utils.ip.IpUtils;
import com.flycms.modules.data.domain.vo.IpAddressVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.flycms.common.utils.StrUtils;
import com.flycms.common.utils.SnowFlakeUtils;
import com.flycms.common.utils.page.Pager;
import com.flycms.modules.data.mapper.IpAddressMapper;
import com.flycms.modules.data.domain.IpAddress;
import com.flycms.modules.data.domain.dto.IpAddressDTO;
import com.flycms.modules.data.service.IIpAddressService;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * IP地址库Service业务层处理
 * 
 * @author admin
 * @date 2020-12-10
 */
@Service
public class IpAddressServiceImpl implements IIpAddressService 
{
    @Autowired
    private IpAddressMapper ipAddressMapper;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增IP地址库
     *
     * @param vo IP地址库
     * @return 结果
     */
    @Override
    public int insertIpAddress(IpAddressVO vo)
    {
        LinkedList<IpAddress> list= ipAddressMapper.findIpAddress(IpUtils.ip2Long(vo.getStartIp()),IpUtils.ip2Long(vo.getEndIp()));
        list.stream().forEach(entity ->{
            if (entity == list.getFirst() ){
                if(entity.getStartIp() < IpUtils.ip2Long(vo.getStartIp())){
                    IpAddress endip=new IpAddress();
                    endip.setId(entity.getId());
                    endip.setEndIp(IpUtils.ip2Long(vo.getStartIp())-1);
                    ipAddressMapper.updateIpAddress(endip);
                }else{
                    ipAddressMapper.deleteIpAddressById(entity.getId());
                }
            }else if(entity==list.getLast()){
                if(entity.getStartIp() < IpUtils.ip2Long(vo.getEndIp())){
                    IpAddress startip=new IpAddress();
                    startip.setId(entity.getId());
                    startip.setStartIp(IpUtils.ip2Long(vo.getEndIp())+1);
                    ipAddressMapper.updateIpAddress(startip);
                }else{
                    ipAddressMapper.deleteIpAddressById(entity.getId());
                }
            }else{
                ipAddressMapper.deleteIpAddressById(entity.getId());
            }
        });
        IpAddress ipAddress = new IpAddress();
        ipAddress.setStartIp(IpUtils.ip2Long(vo.getStartIp()));
        ipAddress.setEndIp(IpUtils.ip2Long(vo.getEndIp()));
        ipAddress.setCountry(vo.getCountry());
        ipAddress.setProvince(vo.getProvince());
        ipAddress.setCity(vo.getCity());
        ipAddress.setCounty(vo.getCounty());
        ipAddress.setAddress(vo.getAddress());
        ipAddress.setLocal(vo.getLocal());
        return ipAddressMapper.insertIpAddress(ipAddress);
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////


    /**
     * 删除IP地址库信息
     *
     * @param id IP地址库ID
     * @return 结果
     */
    @Override
    public int deleteIpAddressById(Long id)
    {
        return ipAddressMapper.deleteIpAddressById(id);
    }


    /**
     * 批量删除IP地址库
     *
     * @param ids 需要删除的IP地址库ID
     * @return 结果
     */
    @Override
    public int deleteIpAddressByIds(Long[] ids)
    {
        return ipAddressMapper.deleteIpAddressByIds(ids);
    }

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改IP地址库
     *
     * @param vo IP地址实体类
     * @return 结果
     */
    @Override
    public int updateIpAddress(IpAddressVO vo)
    {
        LinkedList<IpAddress> list= ipAddressMapper.findIpAddress(IpUtils.ip2Long(vo.getStartIp()),IpUtils.ip2Long(vo.getEndIp()));
        list.stream().forEach(entity ->{
            if (entity == list.getFirst() ){
                if(entity.getStartIp() < IpUtils.ip2Long(vo.getStartIp())){
                    IpAddress endip=new IpAddress();
                    endip.setId(entity.getId());
                    endip.setEndIp(IpUtils.ip2Long(vo.getStartIp())-1);
                    ipAddressMapper.updateIpAddress(endip);
                }else{
                    ipAddressMapper.deleteIpAddressById(entity.getId());
                }
            }else if(entity==list.getLast()){
                if(entity.getStartIp() < IpUtils.ip2Long(vo.getEndIp())){
                    IpAddress startip=new IpAddress();
                    startip.setId(entity.getId());
                    startip.setStartIp(IpUtils.ip2Long(vo.getEndIp())+1);
                    ipAddressMapper.updateIpAddress(startip);
                }else{
                    ipAddressMapper.deleteIpAddressById(entity.getId());
                }
            }else{
                ipAddressMapper.deleteIpAddressById(entity.getId());
            }
        });
        IpAddress ipAddress = new IpAddress();
        ipAddress.setId(vo.getId());
        ipAddress.setStartIp(IpUtils.ip2Long(vo.getStartIp()));
        ipAddress.setEndIp(IpUtils.ip2Long(vo.getEndIp()));
        ipAddress.setCountry(vo.getCountry());
        ipAddress.setProvince(vo.getProvince());
        ipAddress.setCity(vo.getCity());
        ipAddress.setCounty(vo.getCounty());
        ipAddress.setAddress(vo.getAddress());
        ipAddress.setLocal(vo.getLocal());
        return ipAddressMapper.updateIpAddress(ipAddress);
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////


    /**
     * 查询IP地址库
     * 
     * @param id IP地址库ID
     * @return IP地址库
     */
    @Override
    public IpAddressDTO findIpAddressById(Long id)
    {
        IpAddress ipAddress=ipAddressMapper.findIpAddressById(id);
        return BeanConvertor.convertBean(ipAddress,IpAddressDTO.class);
    }

    /**
     * 查询当前大于等于起始段ip并且结束段ip小于当前最近一个ip起始段
     *
     * @param startIp  起始段ip
     * @param endIp   结束段ip
     *
     * @return
     */
    @Override
    public List<IpAddress> findIpAddress(Long startIp,Long endIp)
    {
        return ipAddressMapper.findIpAddress(startIp,endIp);
    }

    /**
     * 查询IP地址
     *
     * @param ip IP地址
     * @return IP地址详细信息
     */
    @Override
    public IpAddressDTO findSearchIpAddress(String ip)
    {
        if(StrUtils.isEmpty(ip)){
            return null;
        }
        IpAddress ipAddress=ipAddressMapper.findSearchIpAddress(IpUtils.ip2Long(ip));
        return BeanConvertor.convertBean(ipAddress,IpAddressDTO.class);
    }

    /**
     * 查询IP地址库列表
     *
     * @param ipAddress IP地址库
     * @return IP地址库
     */
    @Override
    public Pager<IpAddressDTO> selectIpAddressPager(IpAddress ipAddress, Integer page, Integer pageSize, String sort, String order)
    {
        Pager<IpAddressDTO> pager=new Pager(page,pageSize);
        //排序设置
        if (!StrUtils.isEmpty(sort)) {
            Boolean rank = "desc".equals(order) ? true : false;
            pager.addOrderProperty(sort, rank,true);
        }
        //使用limit进行查询翻页
        pager.addLimitProperty(true);
        pager.setEntity(ipAddress);

        List<IpAddress> ipAddressList=ipAddressMapper.selectIpAddressPager(pager);
        List<IpAddressDTO> dtolsit = new ArrayList<IpAddressDTO>();
        ipAddressList.forEach(entity -> {
            IpAddressDTO dto = new IpAddressDTO();
            dto.setId(entity.getId());
            dto.setStartIp(IpUtils.longToIP(entity.getStartIp()));
            dto.setEndIp(IpUtils.longToIP(entity.getEndIp()));
            dto.setCountry(entity.getCountry());
            dto.setProvince(entity.getProvince());
            dto.setCity(entity.getCity());
            dto.setCounty(entity.getCounty());
            dto.setAddress(entity.getAddress());
            dtolsit.add(dto);
        });
        pager.setList(dtolsit);
        pager.setTotal(ipAddressMapper.queryIpAddressTotal(pager));
        return pager;
    }

    /**
     * 查询需要导出的IP地址库列表
     *
     * @param ipAddress IP地址库
     * @return IP地址库集合
     */
    @Override
    public List<IpAddressDTO> exportIpAddressList(IpAddress ipAddress) {
        return BeanConvertor.copyList(ipAddressMapper.exportIpAddressList(ipAddress),IpAddressDTO.class);
    }
}
