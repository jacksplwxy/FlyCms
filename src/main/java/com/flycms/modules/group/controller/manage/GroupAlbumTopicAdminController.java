package com.flycms.modules.group.controller.manage;


import com.flycms.common.constant.UserConstants;
import com.flycms.common.utils.page.Pager;
import com.flycms.common.validator.Order;
import com.flycms.common.validator.Sort;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.flycms.framework.aspectj.lang.annotation.Log;
import com.flycms.framework.aspectj.lang.enums.BusinessType;
import com.flycms.modules.group.domain.GroupAlbumTopic;
import com.flycms.modules.group.domain.dto.GroupAlbumTopicDTO;
import com.flycms.modules.group.service.IGroupAlbumTopicService;
import com.flycms.framework.web.controller.BaseController;
import com.flycms.framework.web.domain.AjaxResult;
import com.flycms.common.utils.poi.ExcelUtil;
import com.flycms.framework.web.page.TableDataInfo;

import java.util.List;

/**
 * 小组专辑帖子关联Controller
 * 
 * @author admin
 * @date 2020-12-16
 */
@RestController
@RequestMapping("/system/group/album/topic")
public class GroupAlbumTopicAdminController extends BaseController
{
    @Autowired
    private IGroupAlbumTopicService groupAlbumTopicService;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////

    /**
     * 新增小组专辑帖子关联
     */
    @PreAuthorize("@ss.hasPermi('group:topic:add')")
    @Log(title = "小组专辑帖子关联", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody GroupAlbumTopic groupAlbumTopic)
    {
        return toAjax(groupAlbumTopicService.insertGroupAlbumTopic(groupAlbumTopic));
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除小组专辑帖子关联
     */
    @PreAuthorize("@ss.hasPermi('group:topic:remove')")
    @Log(title = "小组专辑帖子关联", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(groupAlbumTopicService.deleteGroupAlbumTopicByIds(ids));
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改小组专辑帖子关联
     */
    @PreAuthorize("@ss.hasPermi('group:topic:edit')")
    @Log(title = "小组专辑帖子关联", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody GroupAlbumTopic groupAlbumTopic)
    {
        return toAjax(groupAlbumTopicService.updateGroupAlbumTopic(groupAlbumTopic));
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 查询小组专辑帖子关联列表
     */
    @PreAuthorize("@ss.hasPermi('group:topic:list')")
    @GetMapping("/list")
    public TableDataInfo list(GroupAlbumTopic groupAlbumTopic,
        @RequestParam(defaultValue = "1") Integer pageNum,
        @RequestParam(defaultValue = "10") Integer pageSize,
        @Sort @RequestParam(defaultValue = "id") String sort,
        @Order @RequestParam(defaultValue = "desc") String order)
    {
        Pager<GroupAlbumTopicDTO> pager = groupAlbumTopicService.selectGroupAlbumTopicPager(groupAlbumTopic, pageNum, pageSize, sort, order);
        return getDataTable(pager.getList(),pager.getTotal());
    }

    /**
     * 导出小组专辑帖子关联列表
     */
    @PreAuthorize("@ss.hasPermi('group:topic:export')")
    @Log(title = "小组专辑帖子关联", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(GroupAlbumTopic groupAlbumTopic)
    {
        List<GroupAlbumTopicDTO> groupAlbumTopicList = groupAlbumTopicService.exportGroupAlbumTopicList(groupAlbumTopic);
        ExcelUtil<GroupAlbumTopicDTO> util = new ExcelUtil<GroupAlbumTopicDTO>(GroupAlbumTopicDTO.class);
        return util.exportExcel(groupAlbumTopicList, "topic");
    }

    /**
     * 获取小组专辑帖子关联详细信息
     */
    @PreAuthorize("@ss.hasPermi('group:topic:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(groupAlbumTopicService.findGroupAlbumTopicById(id));
    }

}
