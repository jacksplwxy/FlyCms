package com.flycms.modules.group.domain;
        
import com.flycms.framework.web.domain.TreeEntity;
import lombok.Data;

/**
 * 小组分类对象 fly_group_column
 * 
 * @author admin
 * @date 2020-09-25
 */
@Data
public class GroupColumn extends TreeEntity
{
    private static final long serialVersionUID = 1L;

    /** 分类ID */
    private Long id;
    /** 父分类ID */
    private Long parentId;
    /** 短域名 */
    private String shortUrl;
    /** 分类名称 */
    private String columnName;
    /** 群组个数 */
    private Long countGroup;
}
