package com.flycms.modules.group.domain;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.flycms.framework.aspectj.lang.annotation.Excel;
import com.flycms.framework.web.domain.BaseEntity;
import lombok.Data;

/**
 * 群组和用户对应关系对象 fly_group_user
 * 
 * @author admin
 * @date 2020-09-27
 */
@Data
public class GroupUser implements Serializable
{
    private static final long serialVersionUID = 1L;
    /** ID */
    private Long id;
    /** 用户ID */
    private Long userId;
    /** 群组ID */
    private Long groupId;
    /** 是否管理员 */
    private Integer isadmin;
    /** 是否创始人 */
    private Integer isfounder;
    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    /** 到期时间 */
    @Excel(name = "到期时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date endTime;
    /** 是否禁用状态 */
    private Integer status;
}
