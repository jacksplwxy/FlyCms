package com.flycms.modules.group.domain.dto;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.flycms.framework.aspectj.lang.annotation.Excel;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 帖子分类数据传输对象 fly_group_topic_column
 * 
 * @author admin
 * @date 2020-11-03
 */
@Data
@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
public class GroupTopicColumnDTO implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** 分类ID */
    @Excel(name = "分类ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /** 小组ID */
    @Excel(name = "小组ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long groupId;
    /** 短域名 */
    @Excel(name = "短域名称")
    private String shortUrl;
    /** 用户ID */
    @Excel(name = "用户ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long userId;
    /** 分类名称 */
    @Excel(name = "分类名称")
    private String columnName;
    /** 排序 */
    @Excel(name = "排序")
    private Integer sortOrder;
    /** 统计帖子 */
    @Excel(name = "统计帖子")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long countTopic;
    /** 审核状态 */
    @Excel(name = "审核状态")
    private Integer status;
    /** 创建时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
}
