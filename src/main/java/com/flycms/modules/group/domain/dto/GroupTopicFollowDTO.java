package com.flycms.modules.group.domain.dto;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.flycms.framework.aspectj.lang.annotation.Excel;
import lombok.Data;

import java.io.Serializable;

/**
 * 话题关注数据传输对象 fly_group_topic_follow
 * 
 * @author admin
 * @date 2021-02-01
 */
@Data
@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
public class GroupTopicFollowDTO implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** 自增ID */
    @Excel(name = "自增ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /** 标签ID */
    @Excel(name = "标签ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long topicId;
    /** 用户UID */
    @Excel(name = "用户UID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long userId;

}
