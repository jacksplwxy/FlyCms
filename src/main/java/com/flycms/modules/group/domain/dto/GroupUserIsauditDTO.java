package com.flycms.modules.group.domain.dto;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.flycms.framework.aspectj.lang.annotation.Excel;
import lombok.Data;

import java.io.Serializable;

/**
 * 小组成员审核数据传输对象 fly_group_user_isaudit
 * 
 * @author admin
 * @date 2020-11-24
 */
@Data
@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
public class GroupUserIsauditDTO implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** 用户ID */
    @Excel(name = "用户ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long userId;
    /** 小组ID */
    @Excel(name = "小组ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long groupId;
    /** 审核状态 */
    @Excel(name = "审核状态")
    private Integer status;

}
