package com.flycms.modules.group.service.impl;

import com.flycms.common.utils.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import com.flycms.common.utils.StrUtils;
import com.flycms.common.utils.SnowFlakeUtils;
import com.flycms.common.utils.page.Pager;
import com.flycms.common.constant.UserConstants;
import com.flycms.modules.group.mapper.GroupMapper;
import com.flycms.modules.group.domain.Group;
import com.flycms.modules.group.domain.dto.GroupDTO;
import com.flycms.modules.group.service.IGroupService;

import java.util.ArrayList;
import java.util.List;

/**
 * 群组(小组)Service业务层处理
 * 
 * @author admin
 * @date 2020-09-25
 */
@CacheConfig(cacheNames = "group")
@Service
public class GroupServiceImpl implements IGroupService 
{
    private static final Logger log = LoggerFactory.getLogger(GroupServiceImpl.class);

    @Autowired
    private GroupMapper groupMapper;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增群组(小组)
     *
     * @param group 群组(小组)
     * @return 结果
     */
    @Override
    @CachePut(value = "group", key = "'group_'+#id", unless = "#result eq null")
    public int insertGroup(Group group)
    {
        group.setId(SnowFlakeUtils.nextId());
        group.setCreateTime(DateUtils.getNowDate());
        return groupMapper.insertGroup(group);
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除群组(小组)
     *
     * @param ids 需要删除的群组(小组)ID
     * @return 结果
     */
    @Override
    public int deleteGroupByIds(Long[] ids)
    {
        return groupMapper.deleteGroupByIds(ids);
    }

    /**
     * 删除群组(小组)信息
     *
     * @param id 群组(小组)ID
     * @return 结果
     */
    @Override
    @CacheEvict(value = "group", key = "'group_'+#id")
    public int deleteGroupById(Long id)
    {
        return groupMapper.deleteGroupById(id);
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改群组(小组)
     *
     * @param group 群组(小组)
     * @return 结果
     */
    @Override
    @CachePut(value = "group", key = "'group_'+#group.id", unless = "#result == null")
    public int updateGroup(Group group)
    {
        group.setUpdateTime(DateUtils.getNowDate());
        return groupMapper.updateGroup(group);
    }

    /**
     * 更新小组内发布话题数量
     *
     * @param id 群组(小组)ID
     * @return
     */
    @Override
    public int updateCountTopic(Long id)
    {
        return groupMapper.updateCountTopic(id);
    }

    /**
     * 更新小组加入人数数量
     *
     * @param id 群组(小组)ID
     * @return
     */
    @Override
    public int updateCountUser(Long id)
    {
        return groupMapper.updateCountUser(id);
    }

    /**
     * 更新小组回帖数量
     *
     * @param id 群组(小组)ID
     * @return
     */
    @Override
    public int updateCountComment(Long id)
    {
        return groupMapper.updateCountComment(id);
    }
    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 校验群组名称是否唯一
     *
     * @param group 群组(小组)
     * @return 结果
     */
     @Override
     public String checkGroupGroupNameUnique(Group group)
     {
         int count = groupMapper.checkGroupGroupNameUnique(group);
          if (count > 0)
          {
             return UserConstants.NOT_UNIQUE;
           }
           return UserConstants.UNIQUE;
     }

    /**
     * 查询群组(小组)短域名是否被占用
     *
     * @param shortUrl 群组(小组)
     * @return 结果
     */
    @Override
    public String checkGroupShorturlUnique(String shortUrl)
    {
        int count = groupMapper.checkGroupShorturlUnique(shortUrl);
        if (count > 0)
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 查询群组(小组)
     * 
     * @param id 群组(小组)ID
     * @return 群组(小组)
     */
    @Override
    @Cacheable(value = "group",key = "'group_'+#id",sync=true)
    public Group findGroupById(Long id)
    {
        return groupMapper.findGroupById(id);
    }

    /**
     * 按shortUrl查询群组(小组)信息
     *
     * @param shortUrl 短域名地址
     * @return 群组(小组)
     */
    @Override
    public Group findGroupByShorturl(String shortUrl)
    {
        return groupMapper.findGroupByShorturl(shortUrl);
    }


    /**
     * 查询群组(小组)列表
     *
     * @param group 群组(小组)
     * @return 群组(小组)
     */
    @Override
    public Pager<GroupDTO> selectGroupPager(Group group, Integer page, Integer pageSize, String sort, String order)
    {
        Pager<GroupDTO> pager=new Pager(page,pageSize);
        //排序设置
        if (!StrUtils.isEmpty(sort)) {
            Boolean rank = "desc".equals(order) ? true : false;
            pager.addOrderProperty(sort, rank,true);
        }else{
            pager.addOrderProperty("id", true,true);
        }
        //使用limit进行查询翻页
        pager.addLimitProperty(true);
        pager.setEntity(group);

        List<Group> groupList=groupMapper.selectGroupPager(pager);
        List<GroupDTO> dtolsit = new ArrayList<GroupDTO>();
        groupList.forEach(entity -> {
            GroupDTO dto = new GroupDTO();
            dto.setId(entity.getId());
            dto.setShortUrl(entity.getShortUrl());
            dto.setUserId(entity.getUserId());
            dto.setColumnId(entity.getColumnId());
            dto.setGroupName(entity.getGroupName());
            dto.setGroupDesc(entity.getGroupDesc());
            dto.setPath(entity.getPath());
            dto.setPhoto(entity.getPhoto());
            dto.setCountTopic(entity.getCountTopic());
            dto.setCountTopicToday(entity.getCountTopicToday());
            dto.setCountUser(entity.getCountUser());
            dto.setCountTopicAudit(entity.getCountTopicAudit());
            dto.setCountComment(entity.getCountComment());
            dto.setIsrecommend(entity.getIsrecommend());
            dto.setIsopen(entity.getIsopen());
            dto.setIsaudit(entity.getIsaudit());
            dto.setIspost(entity.getIspost());
            dto.setIsshow(entity.getIsshow());
            dto.setIspostaudit(entity.getIspostaudit());
            dto.setSortOrder(entity.getSortOrder());
            dtolsit.add(dto);
        });
        pager.setList(dtolsit);
        pager.setTotal(groupMapper.queryGroupTotal(pager));
        return pager;
    }

}
