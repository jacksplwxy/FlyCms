package com.flycms.modules.group.service.impl;

import com.flycms.common.utils.DateUtils;

import com.flycms.common.utils.bean.BeanConvertor;
import com.flycms.modules.group.domain.Group;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.flycms.common.utils.StrUtils;
import com.flycms.common.utils.SnowFlakeUtils;
import com.flycms.common.utils.SessionUtils;
import com.flycms.common.utils.page.Pager;
import com.flycms.common.constant.UserConstants;
import com.flycms.modules.group.mapper.GroupTopicFollowMapper;
import com.flycms.modules.group.domain.GroupTopicFollow;
import com.flycms.modules.group.domain.dto.GroupTopicFollowDTO;
import com.flycms.modules.group.service.IGroupTopicFollowService;

import java.util.ArrayList;
import java.util.List;

/**
 * 话题关注Service业务层处理
 * 
 * @author admin
 * @date 2021-02-01
 */
@Service
public class GroupTopicFollowServiceImpl implements IGroupTopicFollowService 
{
    @Autowired
    private GroupTopicFollowMapper groupTopicFollowMapper;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增话题关注
     *
     * @param groupTopicFollow 话题关注
     * @return 结果
     */
    @Override
    public int insertGroupTopicFollow(GroupTopicFollow groupTopicFollow)
    {
        groupTopicFollow.setId(SnowFlakeUtils.nextId());
        groupTopicFollow.setCreateTime(DateUtils.getNowDate());
        return groupTopicFollowMapper.insertGroupTopicFollow(groupTopicFollow);
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除话题关注
     *
     * @param ids 需要删除的话题关注ID
     * @return 结果
     */
    @Override
    public int deleteGroupTopicFollowByIds(Long[] ids)
    {
        return groupTopicFollowMapper.deleteGroupTopicFollowByIds(ids);
    }

    /**
     * 删除话题关注信息
     *
     * @param id 话题关注ID
     * @return 结果
     */
    @Override
    public int deleteGroupTopicFollowById(Long id)
    {
        return groupTopicFollowMapper.deleteGroupTopicFollowById(id);
    }

    /**
     * 按用户id和话题id查询是否关注
     *
     * @param topicId 话题id
     * @param userId 用户id
     * @return
     */
    @Override
    public int deleteTopicUserFollow(Long topicId,Long userId)
    {
        return groupTopicFollowMapper.deleteTopicUserFollow(topicId,userId);
    }
    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改话题关注
     *
     * @param groupTopicFollow 话题关注
     * @return 结果
     */
    @Override
    public int updateGroupTopicFollow(GroupTopicFollow groupTopicFollow)
    {
        return groupTopicFollowMapper.updateGroupTopicFollow(groupTopicFollow);
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 校验话题关注是否唯一
     *
     * @param topicId 话题id
     * @param userId 用户id
     * @return
     */
    @Override
    public String checkTopicFollowUnique(Long topicId,Long userId)
    {
        int count = groupTopicFollowMapper.checkTopicFollowUnique(topicId,userId);
        if (count > 0)
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 查询话题关注
     * 
     * @param id 话题关注ID
     * @return 话题关注
     */
    @Override
    public GroupTopicFollowDTO findGroupTopicFollowById(Long id)
    {
        GroupTopicFollow groupTopicFollow=groupTopicFollowMapper.findGroupTopicFollowById(id);
        return BeanConvertor.convertBean(groupTopicFollow,GroupTopicFollowDTO.class);
    }


    /**
     * 查询话题关注列表
     *
     * @param groupTopicFollow 话题关注
     * @return 话题关注
     */
    @Override
    public Pager<GroupTopicFollowDTO> selectGroupTopicFollowPager(GroupTopicFollow groupTopicFollow, Integer page, Integer pageSize, String sort, String order)
    {
        Pager<GroupTopicFollowDTO> pager=new Pager(page,pageSize);
        //排序设置
        if (!StrUtils.isEmpty(sort)) {
            Boolean rank = "desc".equals(order) ? true : false;
            pager.addOrderProperty(sort, rank,true);
        }
        //使用limit进行查询翻页
        pager.addLimitProperty(true);
        pager.setEntity(groupTopicFollow);

        List<GroupTopicFollow> groupTopicFollowList=groupTopicFollowMapper.selectGroupTopicFollowPager(pager);
        List<GroupTopicFollowDTO> dtolsit = new ArrayList<GroupTopicFollowDTO>();
        groupTopicFollowList.forEach(entity -> {
            GroupTopicFollowDTO dto = new GroupTopicFollowDTO();
            dto.setId(entity.getId());
            dto.setTopicId(entity.getTopicId());
            dto.setUserId(entity.getUserId());
            dtolsit.add(dto);
        });
        pager.setList(dtolsit);
        pager.setTotal(groupTopicFollowMapper.queryGroupTopicFollowTotal(pager));
        return pager;
    }

    /**
     * 查询需要导出的话题关注列表
     *
     * @param groupTopicFollow 话题关注
     * @return 话题关注集合
     */
    @Override
    public List<GroupTopicFollowDTO> exportGroupTopicFollowList(GroupTopicFollow groupTopicFollow) {
        return BeanConvertor.copyList(groupTopicFollowMapper.exportGroupTopicFollowList(groupTopicFollow),GroupTopicFollowDTO.class);
    }
}
