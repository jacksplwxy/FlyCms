package com.flycms.modules.group.service.impl;

import cn.hutool.dfa.WordTree;
import com.flycms.common.constant.UserConstants;
import com.flycms.common.utils.*;
import com.flycms.common.utils.bean.BeanConvertor;
import com.flycms.framework.manager.CacheExpire;
import com.flycms.modules.group.domain.Group;
import com.flycms.modules.group.domain.GroupTopicFollow;
import com.flycms.modules.group.domain.dto.GroupTopicColumnDTO;
import com.flycms.modules.group.service.IGroupService;
import com.flycms.modules.group.service.IGroupTopicColumnService;
import com.flycms.modules.group.service.IGroupTopicFollowService;
import com.flycms.modules.group.service.IGroupTopicService;
import com.flycms.modules.images.service.IImagesService;
import com.flycms.modules.site.service.ISiteService;
import com.flycms.modules.user.domain.User;
import com.flycms.modules.user.service.IUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import com.flycms.common.utils.page.Pager;
import com.flycms.modules.group.mapper.GroupTopicMapper;
import com.flycms.modules.group.domain.GroupTopic;
import com.flycms.modules.group.domain.dto.GroupTopicDTO;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 小组话题Service业务层处理
 * 
 * @author admin
 * @date 2020-09-27
 */
@CacheConfig(cacheNames = "groupTopic")
@Service
public class GroupTopicServiceImpl implements IGroupTopicService
{
    private static final Logger log = LoggerFactory.getLogger(GroupTopicServiceImpl.class);

    @Autowired
    private GroupTopicMapper groupTopicMapper;

    @Autowired
    private IGroupService groupService;

    @Autowired
    private IGroupTopicColumnService groupTopicColumnService;

    @Autowired
    private IUserService userService;

    @Autowired
    private IImagesService imagesService;
    @Autowired
    private ISiteService siteService;

    @Autowired
    private IGroupTopicFollowService groupTopicFollowService;

    /////////////////////////////////
    ///////        增加       ////////
    /////////////////////////////////
    /**
     * 新增小组话题
     *
     * @param groupTopic 接收页面小组话题内容
     * @return 结果
     */
    @Override
    @CachePut(value = "topic", key = "'topic_'+#groupTopic.id", unless = "#result eq null")
    @Transactional
    public int insertGroupTopic(GroupTopic groupTopic)
    {
        groupTopic.setId(SnowFlakeUtils.nextId());
        //用户id
        groupTopic.setUserId(SessionUtils.getUser().getId());
        groupTopic.setCreateTime(DateUtils.getNowDate());

        String content=imagesService.replaceContent(0,groupTopic.getId(),groupTopic.getUserId(),groupTopic.getContent());
        groupTopic.setContent(content);
        int count = groupTopicMapper.insertGroupTopic(groupTopic);
        if(count > 0){
            //添加用户关注话题关联
            GroupTopicFollow groupTopicFollow=new GroupTopicFollow();
            groupTopicFollow.setTopicId(groupTopic.getId());
            groupTopicFollow.setUserId(groupTopic.getUserId());
            groupTopicFollowService.insertGroupTopicFollow(groupTopicFollow);
        }
        return count;
    }

    /**
     * 采集器发布接口新增小组话题
     *
     * @param groupTopic 接收页面小组话题内容
     * @return 结果
     */
    @Override
    @CachePut(value = "topic", key = "'topic_'+#groupTopic.id", unless = "#result eq null")
    @Transactional
    public int insertApiGroupTopic(GroupTopic groupTopic)
    {
        groupTopic.setId(SnowFlakeUtils.nextId());
        groupTopic.setCreateTime(DateUtils.getNowDate());
        String content=imagesService.replaceContent(0,groupTopic.getId(),groupTopic.getUserId(),groupTopic.getContent());
        groupTopic.setContent(content);
        return groupTopicMapper.insertGroupTopic(groupTopic);
    }

    /////////////////////////////////
    ////////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除小组话题
     *
     * @param ids 需要删除的小组话题ID
     * @return 结果
     */
    @Override
    public int deleteGroupTopicByIds(Long[] ids)
    {
        return groupTopicMapper.deleteGroupTopicByIds(ids);
    }

    /**
     * 删除小组话题信息
     *
     * @param id 小组话题ID
     * @return 结果
     */
    @Override
    @CacheEvict(value = "topic", key = "'topic_'+#id")
    public int deleteGroupTopicById(Long id)
    {
        return groupTopicMapper.deleteGroupTopicById(id);
    }


    /////////////////////////////////
    ////////        修改      ////////
    /////////////////////////////////
    /**
     * 修改小组话题
     *
     * @param groupTopic 小组话题
     * @return 结果
     */
    @Override
    @CacheEvict(value = "topic", key = "'topic_'+#groupTopic.id")
    public int updateGroupTopic(GroupTopic groupTopic)
    {
        groupTopic.setUpdateTime(DateUtils.getNowDate());
        if(groupTopic.getContent() != null){
            String content=imagesService.replaceContent(0,groupTopic.getId(),groupTopic.getUserId(),groupTopic.getContent());
            groupTopic.setContent(content);
        }
        return groupTopicMapper.updateGroupTopic(groupTopic);
    }

    /**
     * 修改小组话题假删除
     *
     * @param id 小组话题ID
     * @return 结果
     */
    @Override
    public int updateDeleteGroupTopicById(Long id){
        return groupTopicMapper.updateDeleteGroupTopicById(id);
    }

    /**
     * 更新话题评论数量
     *
     * @param id
     * @return
     */
    @Override
    public int updateCountComment(Long id)
    {
        return groupTopicMapper.updateCountComment(id);
    }

    /**
     * 更新用户话题数量
     *
     * @param userId 用户id
     * @return
     */
    @Override
    public int updateUserCountTopic(Long userId){
        return groupTopicMapper.updateUserCountTopic(userId);
    }

    /**
     * 更新话题被关注数量
     *
     * @param id 话题ID
     * @return
     */
    @Override
    public int updateTopicFollowCount(Long id){
        return groupTopicMapper.updateTopicFollowCount(id);
    }

    /**
     * 更新话题浏览数量
     *
     * @param id 小组话题ID
     * @return
     */
    @Override
    public int updateCountView(Long id){
        return groupTopicMapper.updateCountView(id);
    }

    /**
     * 更新话题排序权重
     *
     * @param weight 权重值
     * @param id  小组话题ID
     * @return
     */
    @Override
    public int updateWeight(Double weight,Long id){
        return groupTopicMapper.updateWeight(weight,id);
    }
    /////////////////////////////////
    ////////        查詢      ////////
    /////////////////////////////////
    /**
     * 校验群组小组话题是否唯一
     *
     * @param groupTopic 小组话题
     * @return 结果
     */
    @Override
    public String checkGroupTopicNameUnique(GroupTopic groupTopic)
    {
        int count = groupTopicMapper.checkGroupTopicNameUnique(groupTopic);
        if (count > 0)
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }


    /**
     * 查询小组话题
     * 
     * @param id 小组话题ID
     * @return 小组话题
     */
    @Override
    @Cacheable(value = "topic",key = "'topic_'+#id",sync=true)
    public GroupTopicDTO findGroupTopicById(Long id)
    {
        GroupTopic  groupTopic=groupTopicMapper.findGroupTopicById(id);
        //替换敏感词
        String sensitiveWord = siteService.selectSite(478279584488632368l).getSensitiveWord();
        if(sensitiveWord != null){
            List<String> result = Arrays.asList(sensitiveWord.split(","));
            WordTree tree = new WordTree();
            for(String key : result){
                key = key.trim();
                tree.addWord(key);
            }
            List<String> matchAll = tree.matchAll(groupTopic.getTitle()+groupTopic.getContent(), -1, true, true);
            groupTopic.setContent(SensitiveWordUtils.replaceSensitiveWord(groupTopic.getContent(),matchAll));
            String title = groupTopic.getTitle();
            groupTopic.setTitle(SensitiveWordUtils.replaceSensitiveWord(title,matchAll));
        }
        return BeanConvertor.convertBean(groupTopic,GroupTopicDTO.class);
    }

    /**
     * 按ID查询内容的上一页
     *
     * @param groupTopic 小组话题
     * @return 小组话题ID和标题
     */
    @Override
    public GroupTopicDTO findGroupTopicByPrePage(GroupTopic groupTopic)
    {
        GroupTopic topic=groupTopicMapper.findGroupTopicByPrePage(groupTopic);
        return BeanConvertor.convertBean(topic,GroupTopicDTO.class);
    }

    /**
     * 按ID查询内容的下一页
     *
     * @param groupTopic 小组话题
     * @return 小组话题ID和标题
     */
    @Override
    public GroupTopicDTO findGroupTopicByNextPage(GroupTopic groupTopic)
    {
        GroupTopic topic=groupTopicMapper.findGroupTopicByNextPage(groupTopic);
        return BeanConvertor.convertBean(topic,GroupTopicDTO.class);
    }

    /**
     * 查询小组话题数量
     *
     * @param groupTopic 小组话题
     * @return 小组话题数量
     */
    @Override
    public int queryGroupTopicTotal(GroupTopic groupTopic) {
        Pager pager=new Pager();
        pager.setEntity(groupTopic);
        return groupTopicMapper.queryGroupTopicTotal(pager);
    }


    /**
     * 查询小组话题列表
     *
     * @param groupTopic 小组话题
     * @return 小组话题
     */
    @Override
    public Pager<GroupTopicDTO> selectGroupTopicPager(GroupTopic groupTopic, Integer page, Integer pageSize, String sort, String order)
    {
        Pager<GroupTopicDTO> pager=new Pager(page,pageSize);
        //排序设置
        if (!StrUtils.isEmpty(sort)) {
            Boolean rank = "desc".equals(order) ? true : false;
            pager.addOrderProperty(sort, rank,true);
        }else{
            pager.addOrderProperty("id", true,true);
        }
        //使用limit进行查询翻页
        pager.addLimitProperty(true);
        pager.setEntity(groupTopic);

        List<GroupTopic> groupTopicList=groupTopicMapper.selectGroupTopicPager(pager);
        List<GroupTopicDTO> dtolsit = new ArrayList<GroupTopicDTO>();
        groupTopicList.forEach(entity -> {
            GroupTopicDTO dto = new GroupTopicDTO();
            dto.setId(entity.getId());
            dto.setColumnId(entity.getColumnId());
            GroupTopicColumnDTO column=groupTopicColumnService.findGroupTopicColumnById(entity.getColumnId());
            if(column != null){
                dto.setColumnName(column.getColumnName());
            }
            Group group=groupService.findGroupById(entity.getGroupId());
            if(group != null){
                dto.setGroupName(group.getGroupName());
            }
            dto.setGroupId(entity.getGroupId());
            dto.setUserId(entity.getUserId());
            User user=userService.findUserById(entity.getUserId());
            if(user != null){
                dto.setNickname(user.getNickname());
            }
            dto.setTitle(entity.getTitle());
            dto.setContent(entity.getContent());
            dto.setCountComment(entity.getCountComment());
            dto.setCountView(entity.getCountView());
            dto.setCountDigg(entity.getCountDigg());
            dto.setCountBurys(entity.getCountBurys());
            dto.setIstop(entity.getIstop());
            dto.setIsclose(entity.getIsclose());
            dto.setIscomment(entity.getIscomment());
            dto.setIscommentshow(entity.getIscommentshow());
            dto.setIsposts(entity.getIsposts());
            dto.setIsaudit(entity.getIsaudit());
            dto.setStatus(entity.getStatus());
            dto.setCreateTime(entity.getCreateTime());
            dtolsit.add(dto);
        });
        pager.setList(dtolsit);
        pager.setTotal(groupTopicMapper.queryGroupTopicTotal(pager));
        return pager;
    }

    /**
     * 查询需要导出的小组话题列表
     *
     * @return 所有小组话题列表
     */
    @Override
    public List<GroupTopicDTO> selectGroupTopicAllList() {
        return BeanConvertor.copyList(groupTopicMapper.selectGroupTopicAllList(),GroupTopicDTO.class);
    }

}
