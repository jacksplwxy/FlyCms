package com.flycms.modules.notify.domain;
import com.flycms.framework.web.domain.BaseEntity;
import lombok.Data;

/**
 * 短信验证码对象 fly_sms_code
 * 
 * @author kaifei sun
 * @date 2020-05-27
 */
@Data
public class SmsCode extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;
    /** 信息类型 */
    private int infoType;
    /** 用户id */
    private Long userId;
    /** 用户名称 */
    private String userName;
    /** 模板id */
    private Long templateId;
    /** 验证码 */
    private String code;
    /** 验证码类型 */
    private int codeType;
    /** 过期时间，以分钟为单位 */
    private int expiresTime;
    /** 激活状态，0未激活，1已激活 */
    private Integer referStatus;
}
