package com.flycms.modules.notify.domain.dto;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import com.flycms.framework.aspectj.lang.annotation.Excel;
import com.flycms.framework.web.domain.BaseEntity;
import lombok.Data;

/**
 * 短信签名数据传输对象 fly_sms_sign
 * 
 * @author admin
 * @date 2020-05-27
 */
@Data
@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
public class SmsSignDto extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    @Excel(name = "ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long id;
    /** 接口ID */
    @Excel(name = "接口ID")
    @JsonSerialize(using = ToStringSerializer.class)
    private Long apiId;
    /** 签名名称 */
    @Excel(name = "签名名称")
    private String signName;
    /** 状态 */
    @Excel(name = "状态")
    private Integer status;
}
