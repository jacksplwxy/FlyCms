package com.flycms.modules.notify.service;

import com.flycms.modules.notify.domain.SmsCode;

public interface IAliyunService {
    /* 短信发送 */
    public Boolean getSendSms(SmsCode smsCode);
}
