package com.flycms.modules.notify.service;

import com.flycms.common.utils.page.Pager;
import com.flycms.modules.notify.domain.Email;
import com.flycms.modules.notify.domain.dto.EmailDTO;

import javax.mail.MessagingException;
import java.util.List;

/**
 * 邮件服务器Service接口
 * 
 * @author admin
 * @date 2020-11-09
 */
public interface IEmailService 
{
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增邮件服务器
     *
     * @param email 邮件服务器
     * @return 结果
     */
    public int insertEmail(Email email);

    /**
     * 给指定用户邮箱发送重置密码邮件
     *
     * @param userEmail
     *        需要发送的用户信息
     * @param code
     *        需要发送的验证码
     * @param tpCode
     *        后台设置的邮件模板key
     * @return
     */
    public void sendEmail(String userEmail,String code, String tpCode);

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除邮件服务器
     *
     * @param mailServers 需要删除的邮件服务器ID
     * @return 结果
     */
    public int deleteEmailByIds(String[] mailServers);

    /**
     * 删除邮件服务器信息
     *
     * @param mailServer 邮件服务器ID
     * @return 结果
     */
    public int deleteEmailById(String mailServer);

    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改邮件服务器
     *
     * @param email 邮件服务器
     * @return 结果
     */
    public int updateEmail(Email email);

    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 校验发件箱KEY是否唯一
     *
     * @param mailServer 发件箱KEY
     * @return 结果
     */
    public String checkEmailMailServerUnique(String mailServer);


    /**
     * 查询邮件服务器
     * 
     * @param mailServer 邮件服务器ID
     * @return 邮件服务器
     */
    public EmailDTO findEmailById(String mailServer);

    /**
     * 查询邮件服务器列表
     * 
     * @param email 邮件服务器
     * @return 邮件服务器集合
     */
    public Pager<EmailDTO> selectEmailPager(Email email, Integer page, Integer limit, String sort, String order);

    /**
     * 查询需要导出的邮件服务器列表
     *
     * @param email 邮件服务器
     * @return 邮件服务器集合
     */
    public List<EmailDTO> exportEmailList(Email email);
}
