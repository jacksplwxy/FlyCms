package com.flycms.modules.notify.service.impl;


import com.flycms.common.constant.UserConstants;
import com.flycms.common.utils.DateUtils;
import com.flycms.common.utils.PlaceholderUtils;
import com.flycms.common.utils.StrUtils;
import com.flycms.common.utils.bean.BeanConvertor;
import com.flycms.common.utils.page.Pager;
import com.flycms.modules.notify.domain.Email;
import com.flycms.modules.notify.domain.EmailTemplet;
import com.flycms.modules.notify.domain.dto.EmailDTO;
import com.flycms.modules.notify.mapper.EmailMapper;
import com.flycms.modules.notify.mapper.EmailTempletMapper;
import com.flycms.modules.notify.service.IEmailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.*;

/**
 * 发送邮件的测试程序
 *
 * @author lwq
 *
 */
@Service
public class EmailServiceImpl implements IEmailService {
    private static final Logger log = LoggerFactory.getLogger(EmailServiceImpl.class);
    @Autowired
    private EmailMapper emailMapper;
    @Autowired
    protected EmailTempletMapper emailTempletMapper;

    // ///////////////////////////////
    // /////       增加       ////////
    // ///////////////////////////////
    /**
     * 新增邮件服务器
     *
     * @param email 邮件服务器
     * @return 结果
     */
    @Override
    public int insertEmail(Email email)
    {
        return emailMapper.insertEmail(email);
    }

    /**
     * 给指定用户邮箱发送重置密码邮件
     *
     * @param userEmail
     *        需要发送的用户信息
     * @param code
     *        需要发送的验证码
     * @param tpCode
     *        后台设置的邮件模板key
     * @return
     * @throws MessagingException
     */
    public void sendEmail(String userEmail,String code, String tpCode)  {
        try {
            EmailTemplet email = emailTempletMapper.findEmailTempletByTpCode(tpCode);
            Map<String, String> map = new HashMap<String, String>();
            map.put("code", code);
            map.put("userEmail", userEmail);
            map.put("createTime", DateUtils.getTime());
            String mailBody = PlaceholderUtils.resolvePlaceholders(email.getContent(), map);

            Email flymail = emailMapper.findEmailByServer("flymail");
            /*
             * PS:某些邮箱服务器要求 SMTP 连接需要使用 SSL安全认证 (为了提高安全性 邮箱支持SSL连接 也可以自己开启)
             *
             * 如无法连接邮件服务器 查看控制台打印的 log 如有类似 "连接失败 要求 SSL安全连接" 等错误
             * 打开下面 注释的六行代码 开启 SSL安全连接
             */
            //1. 创建参数配置 用于连接邮件服务器的参数配置
            Properties props = new Properties();//参数配置
            props.setProperty("mail.transport.protocol", "smtp");//使用的协议(JavaMail规范要求)
            props.setProperty("mail.smtp.port", flymail.getMailSmtpPort());
            props.setProperty("mail.smtp.host", flymail.getMailSmtpServer());//发件人的邮箱的 SMTP 服务器地址
            props.setProperty("mail.smtp.auth", "true");//需要请求认证
            // socketfactory来取代默认的socketfactory
            props.put("mail.smtp.socketFactory.fallback", "false"); // 只处理SSL的连接,对于非SSL的连接不做处理
            props.put("mail.smtp.ssl.enable", true);

            //2. 根据配置创建会话对象 用于和邮件服务器交互
            Session session = Session.getInstance(props);
            session.setDebug(true);//设置为debug模式 可以查看详细的发送 log

            //1. 创建一封邮件
            MimeMessage message = new MimeMessage(session);

            /*
             * from字段 　　--用于指明发件人
             * to字段 　　    --用于指明收件人
             * subject字段  --用于说明邮件主题
             * cc字段 　　   -- 抄送 将邮件发送给收件人的同时抄送给另一个收件人 收件人可以看到邮件抄送给了谁
             * bcc字段 　　 -- 密送 将邮件发送给收件人的同时将邮件秘密发送给另一个收件人 收件人无法看到邮件密送给了谁
             */

            //2. From:发件人的昵称
            message.setFrom(new InternetAddress(flymail.getMailSmtpUsermail(), flymail.getMailUserName(), "UTF-8"));

            //3. To:收件人的昵称(可以增加多个收件人 抄送 密送)
            message.setRecipient(MimeMessage.RecipientType.TO, new InternetAddress(userEmail, "VIP-User", "UTF-8"));

            //4. Subject:邮件主题
            message.setSubject(email.getTitle(), "UTF-8");

            //5. Content:邮件正文(可以使用html标签 浏览器会解析)
            message.setContent(
                    "<html><head><meta charset='utf-8'></head><body>" + mailBody + "</body></html>"
                    , "text/html;charset=UTF-8");
            //6. 设置发件时间
            message.setSentDate(new Date());

            //7. 保存设置
            message.saveChanges();

            //4. 根据Session获取邮件传输对象
            Transport transport = session.getTransport();

            /*
             * 5. 使用邮箱账号和密码连接邮件服务器 此处认证的邮箱必须与 message中的发件人邮箱一致 否则报错
             * PS:成败的判断关键在此 如连接服务器失败 程序会在控制台输出相应失败原因的log日志 仔细查看错误
             * 有些邮箱服务器会返回错误码或查看错误类型的链接 根据给出的错误类型 到对应邮件服务器的帮助网站上查看具体失败原因
             *
             * PS:通常连接失败的原因:
             * 邮箱没有开启 SMTP服务
             * 邮箱密码错误 例如某些邮箱开启了独立密码(设置授权码就必须用授权码作为密码使用)
             * 邮箱服务器要求必须要使用 SSL安全连接
             * 请求过于频繁或其他原因 被邮件服务器拒绝服务网易邮箱严格 换个邮箱开始SMTP使用 或者去邮箱官网查看帮助
             */
            transport.connect(flymail.getMailSmtpUsermail(), flymail.getMailSmtpPassword());

            //6. 发送邮件 发到所有的收件地址  message.getAllRecipients() 获取到的是在创建邮件对象时添加的所有收件人  抄送人
            //密送人
            transport.sendMessage(message, message.getAllRecipients());

            //7. 关闭连接
            transport.close();
        } catch (Exception e) {
            e.printStackTrace();
            log.error("邮件发送错误",e);
        }
    }


    // ///////////////////////////////
    // /////        刪除      ////////
    // ///////////////////////////////
    /**
     * 批量删除邮件服务器
     *
     * @param mailServers 需要删除的邮件服务器ID
     * @return 结果
     */
    @Override
    public int deleteEmailByIds(String[] mailServers)
    {
        return emailMapper.deleteEmailByIds(mailServers);
    }

    /**
     * 删除邮件服务器信息
     *
     * @param mailServer 邮件服务器ID
     * @return 结果
     */
    @Override
    public int deleteEmailById(String mailServer)
    {
        return emailMapper.deleteEmailById(mailServer);
    }
    // ///////////////////////////////
    // /////        修改      ////////
    // ///////////////////////////////
    /**
     * 修改邮件服务器
     *
     * @param email 邮件服务器
     * @return 结果
     */
    @Override
    public int updateEmail(Email email)
    {
        return emailMapper.updateEmail(email);
    }

    // ///////////////////////////////
    // /////        查詢      ////////
    // ///////////////////////////////
    /**
     * 校验发件箱KEY是否唯一
     *
     * @param mailServer 发件箱KEY
     * @return 结果
     */
    @Override
    public String checkEmailMailServerUnique(String mailServer)
    {
        Email email = new Email();
        email.setMailServer(mailServer);
        int count = emailMapper.checkEmailMailServerUnique(email);
        if (count > 0){
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }


    /**
     * 查询邮件服务器
     *
     * @param mailServer 邮件服务器ID
     * @return 邮件服务器
     */
    @Override
    public EmailDTO findEmailById(String mailServer)
    {
        Email  email=emailMapper.findEmailByServer(mailServer);
        return BeanConvertor.convertBean(email,EmailDTO.class);
    }


    /**
     * 查询邮件服务器列表
     *
     * @param email 邮件服务器
     * @return 邮件服务器
     */
    @Override
    public Pager<EmailDTO> selectEmailPager(Email email, Integer page, Integer pageSize, String sort, String order)
    {
        Pager<EmailDTO> pager=new Pager(page,pageSize);
        //排序设置
        if (!StrUtils.isEmpty(sort)) {
            Boolean rank = "desc".equals(order) ? true : false;
            pager.addOrderProperty(sort, rank,true);
        }
        //使用limit进行查询翻页
        pager.addLimitProperty(true);
        pager.setEntity(email);

        List<Email> emailList=emailMapper.selectEmailPager(pager);
        List<EmailDTO> dtolsit = new ArrayList<EmailDTO>();
        emailList.forEach(entity -> {
            EmailDTO dto = new EmailDTO();
            dto.setMailServer(entity.getMailServer());
            dto.setMailSmtpServer(entity.getMailSmtpServer());
            dto.setMailUserName(entity.getMailUserName());
            dto.setMailSmtpUsermail(entity.getMailSmtpUsermail());
            dto.setMailSmtpPassword(entity.getMailSmtpPassword());
            dto.setMailSmtpPort(entity.getMailSmtpPort());
            dtolsit.add(dto);
        });
        pager.setList(dtolsit);
        pager.setTotal(emailMapper.queryEmailTotal(pager));
        return pager;
    }

    /**
     * 查询需要导出的邮件服务器列表
     *
     * @param email 邮件服务器
     * @return 邮件服务器集合
     */
    @Override
    public List<EmailDTO> exportEmailList(Email email) {
        return BeanConvertor.copyList(emailMapper.exportEmailList(email),EmailDTO.class);
    }
}