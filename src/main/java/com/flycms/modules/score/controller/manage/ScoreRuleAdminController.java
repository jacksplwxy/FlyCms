package com.flycms.modules.score.controller.manage;


import com.flycms.common.constant.UserConstants;
import com.flycms.common.utils.SecurityUtils;
import com.flycms.common.utils.page.Pager;
import com.flycms.common.validator.Order;
import com.flycms.common.validator.Sort;
import com.flycms.modules.system.domain.FlyAdmin;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.flycms.framework.aspectj.lang.annotation.Log;
import com.flycms.framework.aspectj.lang.enums.BusinessType;
import com.flycms.modules.score.domain.ScoreRule;
import com.flycms.modules.score.domain.dto.ScoreRuleDTO;
import com.flycms.modules.score.service.IScoreRuleService;
import com.flycms.framework.web.controller.BaseController;
import com.flycms.framework.web.domain.AjaxResult;
import com.flycms.common.utils.poi.ExcelUtil;
import com.flycms.framework.web.page.TableDataInfo;

import java.util.List;

/**
 * 积分规则Controller
 * 
 * @author admin
 * @date 2020-12-01
 */
@RestController
@RequestMapping("/system/score/scoreRule")
public class ScoreRuleAdminController extends BaseController
{
    @Autowired
    private IScoreRuleService scoreRuleService;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////

    /**
     * 新增积分规则
     */
    @PreAuthorize("@ss.hasPermi('score:scoreRule:add')")
    @Log(title = "积分规则", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ScoreRule scoreRule)
    {
        return toAjax(scoreRuleService.insertScoreRule(scoreRule));
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除积分规则
     */
    @PreAuthorize("@ss.hasPermi('score:scoreRule:remove')")
    @Log(title = "积分规则", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(scoreRuleService.deleteScoreRuleByIds(ids));
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改积分规则
     */
    @PreAuthorize("@ss.hasPermi('score:scoreRule:edit')")
    @Log(title = "积分规则", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ScoreRule scoreRule)
    {
        return toAjax(scoreRuleService.updateScoreRule(scoreRule));
    }

    /**
     * 状态修改
     */
    @PreAuthorize("@ss.hasPermi('system:admin:edit')")
    @Log(title = "用户管理", businessType = BusinessType.UPDATE)
    @PutMapping("/changeStatus")
    public AjaxResult changeStatus(@RequestBody ScoreRule scoreRule)
    {
        return toAjax(scoreRuleService.updateScoreRuleEnabled(scoreRule.getStatus(),scoreRule.getId()));
    }
    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 查询积分规则列表
     */
    @PreAuthorize("@ss.hasPermi('score:scoreRule:list')")
    @GetMapping("/list")
    public TableDataInfo list(ScoreRule scoreRule,
        @RequestParam(defaultValue = "1") Integer pageNum,
        @RequestParam(defaultValue = "10") Integer pageSize,
        @Sort @RequestParam(defaultValue = "id") String sort,
        @Order @RequestParam(defaultValue = "desc") String order)
    {
        Pager<ScoreRuleDTO> pager = scoreRuleService.selectScoreRulePager(scoreRule, pageNum, pageSize, sort, order);
        return getDataTable(pager.getList(),pager.getTotal());
    }

    /**
     * 导出积分规则列表
     */
    @PreAuthorize("@ss.hasPermi('score:scoreRule:export')")
    @Log(title = "积分规则", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(ScoreRule scoreRule)
    {
        List<ScoreRuleDTO> scoreRuleList = scoreRuleService.exportScoreRuleList(scoreRule);
        ExcelUtil<ScoreRuleDTO> util = new ExcelUtil<ScoreRuleDTO>(ScoreRuleDTO.class);
        return util.exportExcel(scoreRuleList, "scoreRule");
    }

    /**
     * 获取积分规则详细信息
     */
    @PreAuthorize("@ss.hasPermi('score:scoreRule:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(scoreRuleService.findScoreRuleById(id));
    }

}
