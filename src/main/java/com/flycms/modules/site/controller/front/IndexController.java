package com.flycms.modules.site.controller.front;

import com.flycms.modules.site.service.ISiteService;
import com.flycms.framework.web.controller.BaseController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

/**
 * 官网设置Controller
 * 
 * @author admin
 * @date 2020-07-08
 */
@Controller
public class IndexController extends BaseController
{
    @Autowired
    private ISiteService siteService;

    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////

    /**
     * 首页
     *
     * @return
     */
    @RequestMapping(value = {"/" , "/index"})
    public String index(ModelMap modelMap){
        return theme.getPcTemplate("index");
    }
}
