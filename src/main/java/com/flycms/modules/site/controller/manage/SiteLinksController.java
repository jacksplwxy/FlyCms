package com.flycms.modules.site.controller.manage;

import com.flycms.common.constant.UserConstants;
import com.flycms.common.utils.page.Pager;
import com.flycms.common.utils.poi.ExcelUtil;
import com.flycms.common.validator.Order;
import com.flycms.common.validator.Sort;
import com.flycms.framework.aspectj.lang.annotation.Log;
import com.flycms.framework.aspectj.lang.enums.BusinessType;
import com.flycms.modules.site.domain.SiteLinks;
import com.flycms.modules.site.service.ISiteLinksService;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.flycms.modules.site.domain.dto.SiteLinksDTO;
import com.flycms.framework.web.controller.BaseController;
import com.flycms.framework.web.domain.AjaxResult;
import com.flycms.framework.web.page.TableDataInfo;

/**
 * 友情链接Controller
 * 
 * @author admin
 * @date 2020-07-08
 */
@RestController
@RequestMapping("/system/site/links")
public class SiteLinksController extends BaseController
{
    @Autowired
    private ISiteLinksService siteLinksService;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////

    /**
     * 新增友情链接
     */
    @PreAuthorize("@ss.hasPermi('site:links:add')")
    @Log(title = "友情链接", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody SiteLinks siteLinks)
    {
        if (UserConstants.NOT_UNIQUE.equals(siteLinksService.checkSiteLinksLinkNameUnique(siteLinks)))
        {
            return AjaxResult.error("新增友情链接'" + siteLinks.getLinkName() + "'失败，网站名称已存在");
        }
        return toAjax(siteLinksService.insertSiteLinks(siteLinks));
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 删除友情链接
     */
    @PreAuthorize("@ss.hasPermi('site:links:remove')")
    @Log(title = "友情链接", businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(siteLinksService.deleteSiteLinksByIds(ids));
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改友情链接
     */
    @PreAuthorize("@ss.hasPermi('site:links:edit')")
    @Log(title = "友情链接", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody SiteLinks siteLinks)
    {
        return toAjax(siteLinksService.updateSiteLinks(siteLinks));
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 查询友情链接列表
     */
    @PreAuthorize("@ss.hasPermi('site:links:list')")
    @GetMapping("/list")
    public TableDataInfo list(SiteLinks siteLinks,
        @RequestParam(defaultValue = "1") Integer pageNum,
        @RequestParam(defaultValue = "10") Integer pageSize,
        @Sort @RequestParam(defaultValue = "id") String sort,
        @Order @RequestParam(defaultValue = "desc") String order)
    {
        Pager<SiteLinksDTO> pager = siteLinksService.selectSiteLinksPager(siteLinks, pageNum, pageSize, sort, order);
        return getDataTable(pager.getList(),pager.getTotal());
    }

    /**
     * 导出友情链接列表
     */
    @PreAuthorize("@ss.hasPermi('site:links:export')")
    @Log(title = "友情链接", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult export(SiteLinks siteLinks,
            @RequestParam(defaultValue = "1") Integer pageNum,
            @RequestParam(defaultValue = "10") Integer pageSize,
            @Sort @RequestParam(defaultValue = "id") String sort,
            @Order @RequestParam(defaultValue = "desc") String order)
    {
        Pager<SiteLinksDTO> pager = siteLinksService.selectSiteLinksPager(siteLinks, pageNum, pageSize, sort, order);
        ExcelUtil<SiteLinksDTO> util = new ExcelUtil<SiteLinksDTO>(SiteLinksDTO.class);
        return util.exportExcel(pager.getList(), "links");
    }

    /**
     * 获取友情链接详细信息
     */
    @PreAuthorize("@ss.hasPermi('site:links:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return AjaxResult.success(siteLinksService.selectSiteLinksById(id));
    }
}
