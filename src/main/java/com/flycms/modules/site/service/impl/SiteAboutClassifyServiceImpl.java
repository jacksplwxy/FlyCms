package com.flycms.modules.site.service.impl;

import com.flycms.common.utils.DateUtils;

import com.flycms.common.utils.bean.BeanConvertor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.flycms.common.utils.StrUtils;
import com.flycms.common.utils.SnowFlakeUtils;
import com.flycms.common.utils.page.Pager;
import com.flycms.common.constant.UserConstants;
import com.flycms.modules.site.mapper.SiteAboutClassifyMapper;
import com.flycms.modules.site.domain.SiteAboutClassify;
import com.flycms.modules.site.domain.dto.SiteAboutClassifyDTO;
import com.flycms.modules.site.service.ISiteAboutClassifyService;

import java.util.ArrayList;
import java.util.List;

/**
 * 关于我们分类Service业务层处理
 * 
 * @author admin
 * @date 2021-01-27
 */
@Service
public class SiteAboutClassifyServiceImpl implements ISiteAboutClassifyService 
{
    @Autowired
    private SiteAboutClassifyMapper siteAboutClassifyMapper;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增关于我们分类
     *
     * @param siteAboutClassify 关于我们分类
     * @return 结果
     */
    @Override
    public int insertSiteAboutClassify(SiteAboutClassify siteAboutClassify)
    {
        siteAboutClassify.setId(SnowFlakeUtils.nextId());
        siteAboutClassify.setCreateTime(DateUtils.getNowDate());
        return siteAboutClassifyMapper.insertSiteAboutClassify(siteAboutClassify);
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除关于我们分类
     *
     * @param ids 需要删除的关于我们分类ID
     * @return 结果
     */
    @Override
    public int deleteSiteAboutClassifyByIds(Long[] ids)
    {
        return siteAboutClassifyMapper.deleteSiteAboutClassifyByIds(ids);
    }

    /**
     * 删除关于我们分类信息
     *
     * @param id 关于我们分类ID
     * @return 结果
     */
    @Override
    public int deleteSiteAboutClassifyById(Long id)
    {
        return siteAboutClassifyMapper.deleteSiteAboutClassifyById(id);
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改关于我们分类
     *
     * @param siteAboutClassify 关于我们分类
     * @return 结果
     */
    @Override
    public int updateSiteAboutClassify(SiteAboutClassify siteAboutClassify)
    {
        siteAboutClassify.setUpdateTime(DateUtils.getNowDate());
        return siteAboutClassifyMapper.updateSiteAboutClassify(siteAboutClassify);
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////

    /**
     * 查询关于我们分类
     * 
     * @param id 关于我们分类ID
     * @return 关于我们分类
     */
    @Override
    public SiteAboutClassifyDTO findSiteAboutClassifyById(Long id)
    {
        SiteAboutClassify siteAboutClassify=siteAboutClassifyMapper.findSiteAboutClassifyById(id);
        return BeanConvertor.convertBean(siteAboutClassify,SiteAboutClassifyDTO.class);
    }


    /**
     * 查询关于我们分类列表
     *
     * @param siteAboutClassify 关于我们分类
     * @return 关于我们分类
     */
    @Override
    public Pager<SiteAboutClassifyDTO> selectSiteAboutClassifyPager(SiteAboutClassify siteAboutClassify, Integer page, Integer pageSize, String sort, String order)
    {
        Pager<SiteAboutClassifyDTO> pager=new Pager(page,pageSize);
        //排序设置
        if (!StrUtils.isEmpty(sort)) {
            Boolean rank = "desc".equals(order) ? true : false;
            pager.addOrderProperty(sort, rank,true);
        }
        //使用limit进行查询翻页
        pager.addLimitProperty(true);
        pager.setEntity(siteAboutClassify);

        List<SiteAboutClassify> siteAboutClassifyList=siteAboutClassifyMapper.selectSiteAboutClassifyPager(pager);
        List<SiteAboutClassifyDTO> dtolsit = new ArrayList<SiteAboutClassifyDTO>();
        siteAboutClassifyList.forEach(entity -> {
            SiteAboutClassifyDTO dto = new SiteAboutClassifyDTO();
            dto.setId(entity.getId());
            dto.setClassifyName(entity.getClassifyName());
            dto.setParentId(entity.getParentId());
            dtolsit.add(dto);
        });
        pager.setList(dtolsit);
        pager.setTotal(siteAboutClassifyMapper.querySiteAboutClassifyTotal(pager));
        return pager;
    }

    /**
     * 查询需要导出的关于我们分类列表
     *
     * @param siteAboutClassify 关于我们分类
     * @return 关于我们分类集合
     */
    @Override
    public List<SiteAboutClassifyDTO> exportSiteAboutClassifyList(SiteAboutClassify siteAboutClassify) {
        return BeanConvertor.copyList(siteAboutClassifyMapper.exportSiteAboutClassifyList(siteAboutClassify),SiteAboutClassifyDTO.class);
    }
}
