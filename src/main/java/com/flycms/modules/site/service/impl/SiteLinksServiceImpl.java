package com.flycms.modules.site.service.impl;

import com.flycms.common.constant.UserConstants;
import com.flycms.common.utils.DateUtils;
import com.flycms.common.utils.SnowFlakeUtils;
import com.flycms.common.utils.StrUtils;
import com.flycms.common.utils.page.Pager;
import com.flycms.modules.site.domain.SiteLinks;
import com.flycms.modules.site.mapper.SiteLinksMapper;
import com.flycms.modules.site.service.ISiteLinksService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.flycms.modules.site.domain.dto.SiteLinksDTO;

import java.util.ArrayList;
import java.util.List;

/**
 * 友情链接Service业务层处理
 * 
 * @author admin
 * @date 2020-07-08
 */
@Service
public class SiteLinksServiceImpl implements ISiteLinksService
{
    @Autowired
    private SiteLinksMapper siteLinksMapper;
    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增友情链接
     *
     * @param siteLinks 友情链接
     * @return 结果
     */
    @Override
    public int insertSiteLinks(SiteLinks siteLinks)
    {
        siteLinks.setId(SnowFlakeUtils.nextId());
        siteLinks.setCreateTime(DateUtils.getNowDate());
        return siteLinksMapper.insertSiteLinks(siteLinks);
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除友情链接
     *
     * @param ids 需要删除的友情链接ID
     * @return 结果
     */
    @Override
    public int deleteSiteLinksByIds(Long[] ids)
    {
        return siteLinksMapper.deleteSiteLinksByIds(ids);
    }

    /**
     * 删除友情链接信息
     *
     * @param id 友情链接ID
     * @return 结果
     */
    @Override
    public int deleteSiteLinksById(Long id)
    {
        return siteLinksMapper.deleteSiteLinksById(id);
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改友情链接
     *
     * @param siteLinks 友情链接
     * @return 结果
     */
    @Override
    public int updateSiteLinks(SiteLinks siteLinks)
    {
        return siteLinksMapper.updateSiteLinks(siteLinks);
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 校验网站名称是否唯一
     *
     * @param siteLinks 友情链接
     * @return 结果
     */
     @Override
     public String checkSiteLinksLinkNameUnique(SiteLinks siteLinks)
     {
         int count = siteLinksMapper.checkSiteLinksLinkNameUnique(siteLinks);
          if (count > 0)
          {
             return UserConstants.NOT_UNIQUE;
           }
           return UserConstants.UNIQUE;
     }


    /**
     * 查询友情链接
     * 
     * @param id 友情链接ID
     * @return 友情链接
     */
    @Override
    public SiteLinks selectSiteLinksById(Long id)
    {
        return siteLinksMapper.selectSiteLinksById(id);
    }


    /**
     * 查询友情链接列表
     *
     * @param siteLinks 友情链接
     * @return 友情链接
     */
    @Override
    public Pager<SiteLinksDTO> selectSiteLinksPager(SiteLinks siteLinks, Integer page, Integer pageSize, String sort, String order)
    {
        Pager<SiteLinksDTO> pager=new Pager(page,pageSize);
        //排序设置
        if (!StrUtils.isEmpty(sort)) {
            Boolean rank = "desc".equals(order) ? true : false;
            pager.addOrderProperty(sort, rank,true);
        }
        //使用limit进行查询翻页
        pager.addLimitProperty(true);
        pager.setEntity(siteLinks);

        List<SiteLinks> siteLinksList=siteLinksMapper.selectSiteLinksPager(pager);
        List<SiteLinksDTO> dtolsit = new ArrayList<SiteLinksDTO>();
        siteLinksList.forEach(entity -> {
            SiteLinksDTO dto = new SiteLinksDTO();
            dto.setId(entity.getId());
            dto.setLinkType(entity.getLinkType());
            dto.setLinkName(entity.getLinkName());
            dto.setLinkUrl(entity.getLinkUrl());
            dto.setLinkLogo(entity.getLinkLogo());
            dto.setIsShow(entity.getIsShow());
            dto.setSortOrder(entity.getSortOrder());
            dtolsit.add(dto);
        });
        pager.setList(dtolsit);
        pager.setTotal(siteLinksMapper.querySiteLinksTotal(pager));
        return pager;
    }

}
