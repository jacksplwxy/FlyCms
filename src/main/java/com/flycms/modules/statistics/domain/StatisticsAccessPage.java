package com.flycms.modules.statistics.domain;

import com.flycms.framework.web.domain.BaseEntity;
import lombok.Data;

import java.util.Date;

/**
 * 受访页面日报对象 fly_statistics_access_page
 * 
 * @author admin
 * @date 2021-01-12
 */
@Data
public class StatisticsAccessPage extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** id */
    private Long id;
    /** 新客户 */
    private Integer isNewVisitor;
    /** 统计日期 */
    private Date statisticsDay;
    /** 来源类型 */
    private Integer sourceType;
    /** 页面类型 */
    private Integer urlType;
    /** 页面地址 */
    private String url;
    /** 浏览量 */
    private Integer pvs;
    /** 访客数 */
    private Integer uvs;
    /** 贡献流量 */
    private Integer flows;
    /** 时长 */
    private Long accessHoureLong;
    /** 访问次数 */
    private Integer onlyOnePv;
    /** 删除标识 */
    private Integer deleted;
    /** 查询时间类型，1今天，2昨天，3本周，4本月，5上个月，6本年 */
    private String time;
}
