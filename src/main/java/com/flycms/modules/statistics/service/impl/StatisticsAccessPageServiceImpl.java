package com.flycms.modules.statistics.service.impl;


import com.flycms.common.utils.bean.BeanConvertor;
import com.flycms.modules.statistics.domain.StatisticsAccessRecord;
import com.flycms.modules.statistics.service.IStatisticsAccessRecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.flycms.common.utils.StrUtils;
import com.flycms.common.utils.SnowFlakeUtils;
import com.flycms.common.utils.page.Pager;
import com.flycms.common.constant.UserConstants;
import com.flycms.modules.statistics.mapper.StatisticsAccessPageMapper;
import com.flycms.modules.statistics.domain.StatisticsAccessPage;
import com.flycms.modules.statistics.domain.dto.StatisticsAccessPageDTO;
import com.flycms.modules.statistics.service.IStatisticsAccessPageService;

import java.util.ArrayList;
import java.util.List;

/**
 * 受访页面日报Service业务层处理
 * 
 * @author admin
 * @date 2021-01-12
 */
@Service
public class StatisticsAccessPageServiceImpl implements IStatisticsAccessPageService 
{
    @Autowired
    private StatisticsAccessPageMapper statisticsAccessPageMapper;

    @Autowired
    private IStatisticsAccessRecordService statisticsAccessRecordService;

    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增受访页面日报
     *
     * @return 结果
     */
    @Override
    public void insertStatisticsAccessPage()
    {
        int j=statisticsAccessRecordService.queryStatisticsAccessRecordCount(2);
        int c = j%1000==0?j/1000:j/1000+1;
        for (int i=0; i<c;i++) {
            List<StatisticsAccessRecord> record=statisticsAccessRecordService.selectStatisticsAccessRecordList(2,i*1000,1000);
            for (StatisticsAccessRecord info:record) {
                if(statisticsAccessPageMapper.checkAccessPageNameUnique(info.getAccessUrl(),2)==0){
                    StatisticsAccessPage page=new StatisticsAccessPage();
                    page.setId(SnowFlakeUtils.nextId());
                    page.setUvs(info.getCounts());
                    if(info.getCookieId()!=null){
                        int cookie=statisticsAccessRecordService.queryStatisticsAccessRecordCookieIdCount(info.getCookieId());
                        if(cookie >= 2){
                            page.setIsNewVisitor(1);
                        }else{
                            page.setIsNewVisitor(0);
                        }
                    }else{
                        page.setIsNewVisitor(0);
                    }
                    //page.setSourceType(1);
                    page.setPvs(info.getCounts());
                    page.setUrl(info.getAccessUrl());
                    page.setStatisticsDay(info.getCreateTime());
                    statisticsAccessPageMapper.insertStatisticsAccessPage(page);
                }
            }
        }
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除受访页面日报
     *
     * @param ids 需要删除的受访页面日报ID
     * @return 结果
     */
    @Override
    public int deleteStatisticsAccessPageByIds(Long[] ids)
    {
        return statisticsAccessPageMapper.deleteStatisticsAccessPageByIds(ids);
    }

    /**
     * 删除受访页面日报信息
     *
     * @param id 受访页面日报ID
     * @return 结果
     */
    @Override
    public int deleteStatisticsAccessPageById(Long id)
    {
        return statisticsAccessPageMapper.deleteStatisticsAccessPageById(id);
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改受访页面日报
     *
     * @param statisticsAccessPage 受访页面日报
     * @return 结果
     */
    @Override
    public int updateStatisticsAccessPage(StatisticsAccessPage statisticsAccessPage)
    {
        return statisticsAccessPageMapper.updateStatisticsAccessPage(statisticsAccessPage);
    }


    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 校验查询受访页面是否唯一
     *
     * @param url
     * @param time
     * @return
     */
    public int checkAccessPageNameUnique(String url, int time){
        return statisticsAccessPageMapper.checkAccessPageNameUnique(url,time);
    }

    /**
     * 按时间查询页面统计记录
     *
     * @param time
     * @return
     */
    @Override
    public int queryStatisticsAccessPageCount(Integer time){
        return statisticsAccessPageMapper.queryStatisticsAccessPageCount(time);
    }

    /**
     * 查询受访页面日报
     * 
     * @param id 受访页面日报ID
     * @return 受访页面日报
     */
    @Override
    public StatisticsAccessPageDTO findStatisticsAccessPageById(Long id)
    {
        StatisticsAccessPage statisticsAccessPage=statisticsAccessPageMapper.findStatisticsAccessPageById(id);
        return BeanConvertor.convertBean(statisticsAccessPage,StatisticsAccessPageDTO.class);
    }


    /**
     * 查询受访页面日报列表
     *
     * @param statisticsAccessPage 受访页面日报
     * @return 受访页面日报
     */
    @Override
    public Pager<StatisticsAccessPageDTO> selectStatisticsAccessPagePager(StatisticsAccessPage statisticsAccessPage, Integer page, Integer limit, String sort, String order)
    {
        Pager<StatisticsAccessPageDTO> pager=new Pager(page,limit);
        //排序设置
        if (!StrUtils.isEmpty(sort)) {
            Boolean rank = "desc".equals(order) ? true : false;
            pager.addOrderProperty(sort, rank,true);
        }
        //使用limit进行查询翻页
        pager.addLimitProperty(true);
        pager.setEntity(statisticsAccessPage);

        List<StatisticsAccessPage> statisticsAccessPageList=statisticsAccessPageMapper.selectStatisticsAccessPagePager(pager);
        List<StatisticsAccessPageDTO> dtolsit = new ArrayList<StatisticsAccessPageDTO>();
        statisticsAccessPageList.forEach(entity -> {
            StatisticsAccessPageDTO dto = new StatisticsAccessPageDTO();
            dto.setId(entity.getId());
            dto.setIsNewVisitor(entity.getIsNewVisitor());
            dto.setStatisticsDay(entity.getStatisticsDay());
            dto.setSourceType(entity.getSourceType());
            dto.setUrlType(entity.getUrlType());
            dto.setUrl(entity.getUrl());
            dto.setPvs(entity.getPvs());
            dto.setUvs(entity.getUvs());
            dto.setFlows(entity.getFlows());
            dto.setAccessHoureLong(entity.getAccessHoureLong());
            dto.setOnlyOnePv(entity.getOnlyOnePv());
            dto.setDeleted(entity.getDeleted());
            dtolsit.add(dto);
        });
        pager.setList(dtolsit);
        pager.setTotal(statisticsAccessPageMapper.queryStatisticsAccessPageTotal(pager));
        return pager;
    }

    /**
     * 查询需要导出的受访页面日报列表
     *
     * @param statisticsAccessPage 受访页面日报
     * @return 受访页面日报集合
     */
    @Override
    public List<StatisticsAccessPageDTO> exportStatisticsAccessPageList(StatisticsAccessPage statisticsAccessPage) {
        return BeanConvertor.copyList(statisticsAccessPageMapper.exportStatisticsAccessPageList(statisticsAccessPage),StatisticsAccessPageDTO.class);
    }

    /**
     * 受访页面列表
     *
     * @param time
     * @param offset
     * @param rows
     * @return
     */
    @Override
    public List<StatisticsAccessPage> getAccessPageList(Integer time,Integer offset, Integer rows)
    {
        return statisticsAccessPageMapper.getAccessPageList(time,offset,rows);
    }
}
