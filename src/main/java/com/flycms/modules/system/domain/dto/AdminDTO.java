package com.flycms.modules.system.domain.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import lombok.Data;

import java.util.List;

@Data
@JsonInclude(value = JsonInclude.Include.NON_EMPTY)
public class AdminDTO {
    private static final long serialVersionUID = 1L;

    /** 用户ID */
    @JsonSerialize(using = ToStringSerializer.class)
    private Long adminId;

    /** 用户账号 */
    private String adminName;

    /** 用户昵称 */
    private String nickName;

}
