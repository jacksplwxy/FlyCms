package com.flycms.modules.system.mapper;

import java.util.List;

import com.flycms.modules.system.domain.FlyRole;
import org.apache.ibatis.annotations.Param;
import com.flycms.modules.system.domain.FlyAdminRole;
import org.springframework.stereotype.Repository;

/**
 * 用户与角色关联表 数据层
 * 
 * @author kaifei sun
 */
@Repository
public interface FlyAdminRoleMapper {
    /////////////////////////////////
    ///////        增加       ////////
    /////////////////////////////////

    /**
     * 批量新增用户角色信息
     *
     * @param userRoleList 用户角色列表
     * @return 结果
     */
    public int batchAdminRole(List<FlyAdminRole> userRoleList);

    /////////////////////////////////
    ///////        刪除       ////////
    /////////////////////////////////

    /**
     * 通过用户ID删除用户和角色关联
     *
     * @param adminId 用户ID
     * @return 结果
     */
    public int deleteAdminRoleByAdminId(Long adminId);

    /**
     * 批量删除用户和角色关联
     *
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteAdminRole(Long[] ids);

    /**
     * 删除用户和角色关联信息
     *
     * @param userRole 用户和角色关联信息
     * @return 结果
     */
    public int deleteAdminRoleInfo(FlyAdminRole userRole);

    /**
     * 批量取消授权用户角色
     *
     * @param roleId 角色ID
     * @param adminIds 需要删除的用户数据ID
     * @return 结果
     */
    public int deleteAdminRoleInfos(@Param("roleId") Long roleId, @Param("adminIds") Long[] adminIds);

    /////////////////////////////////
    ///////        修改       ////////
    /////////////////////////////////


    /////////////////////////////////
    ///////        查詢       ////////
    /////////////////////////////////
    /**
     * 通过角色ID查询角色使用数量
     * 
     * @param roleId 角色ID
     * @return 结果
     */
    public int countAdminRoleByRoleId(Long roleId);

    /**
     * 按用户id查询所有权限
     *
     * @param adminId
     * @return
     */
    public List<FlyRole> selectAdminRoleByAdminId(Long adminId);
}
