package com.flycms.modules.system.mapper;

import java.util.List;
import com.flycms.modules.system.domain.FlyNotice;
import org.springframework.stereotype.Repository;

/**
 * 通知公告表 数据层
 * 
 * @author kaifei sun
 */
@Repository
public interface FlyNoticeMapper
{
    /////////////////////////////////
    ////////       增加       ////////
    /////////////////////////////////
    /**
     * 新增公告
     *
     * @param notice 公告信息
     * @return 结果
     */
    public int insertNotice(FlyNotice notice);
    /////////////////////////////////
    ////////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除公告
     *
     * @param noticeId 公告ID
     * @return 结果
     */
    public int deleteNoticeById(Long noticeId);

    /**
     * 批量删除公告信息
     *
     * @param noticeIds 需要删除的公告ID
     * @return 结果
     */
    public int deleteNoticeByIds(Long[] noticeIds);
    /////////////////////////////////
    ////////        修改      ////////
    /////////////////////////////////
    /**
     * 修改公告
     *
     * @param notice 公告信息
     * @return 结果
     */
    public int updateNotice(FlyNotice notice);
    /////////////////////////////////
    ////////        查詢      ////////
    /////////////////////////////////
    /**
     * 查询公告信息
     * 
     * @param noticeId 公告ID
     * @return 公告信息
     */
    public FlyNotice selectNoticeById(Long noticeId);

    /**
     * 查询公告列表
     * 
     * @param notice 公告信息
     * @return 公告集合
     */
    public List<FlyNotice> selectNoticeList(FlyNotice notice);

}