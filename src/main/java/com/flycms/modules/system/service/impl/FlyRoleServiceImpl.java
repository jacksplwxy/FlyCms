package com.flycms.modules.system.service.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.flycms.common.constant.UserConstants;
import com.flycms.common.exception.CustomException;
import com.flycms.common.utils.SnowFlakeUtils;
import com.flycms.common.utils.StrUtils;
import com.flycms.common.utils.page.Pager;
import com.flycms.framework.aspectj.lang.annotation.DataScope;
import com.flycms.modules.system.domain.FlyRole;
import com.flycms.modules.system.domain.FlyRoleDept;
import com.flycms.modules.system.domain.FlyRoleMenu;
import com.flycms.modules.system.mapper.FlyAdminRoleMapper;
import com.flycms.modules.system.mapper.FlyRoleDeptMapper;
import com.flycms.modules.system.mapper.FlyRoleMapper;
import com.flycms.modules.system.mapper.FlyRoleMenuMapper;
import com.flycms.modules.system.domain.dto.RoleQueryDTO;
import com.flycms.modules.system.domain.vo.FlyRoleVo;
import com.flycms.modules.system.service.IFlyRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 角色 业务层处理
 * 
 * @author kaifei sun
 */
@Service
public class FlyRoleServiceImpl implements IFlyRoleService
{
    @Autowired
    private FlyRoleMapper roleMapper;

    @Autowired
    private FlyRoleMenuMapper roleMenuMapper;

    @Autowired
    private FlyAdminRoleMapper userRoleMapper;

    @Autowired
    private FlyRoleDeptMapper roleDeptMapper;


    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增保存角色信息
     *
     * @param role 角色信息
     * @return 结果
     */
    @Override
    @Transactional
    public int insertRole(FlyRole role)
    {
        // 新增角色信息
        role.setRoleId(SnowFlakeUtils.nextId());
        roleMapper.insertRole(role);
        return insertRoleMenu(role);
    }

    /**
     * 修改数据权限信息
     *
     * @param role 角色信息
     * @return 结果
     */
    @Override
    @Transactional
    public int authDataScope(FlyRole role)
    {
        // 修改角色信息
        roleMapper.updateRole(role);
        // 删除角色与部门关联
        roleDeptMapper.deleteRoleDeptByRoleId(role.getRoleId());
        // 新增角色和部门信息（数据权限）
        return insertRoleDept(role);
    }

    /**
     * 新增角色菜单信息
     *
     * @param role 角色对象
     */
    public int insertRoleMenu(FlyRole role)
    {
        int rows = 1;
        // 新增用户与角色管理
        List<FlyRoleMenu> list = new ArrayList<FlyRoleMenu>();
        for (Long menuId : role.getMenuIds())
        {
            FlyRoleMenu rm = new FlyRoleMenu();
            rm.setRoleId(role.getRoleId());
            rm.setMenuId(menuId);
            list.add(rm);
        }
        if (list.size() > 0)
        {
            rows = roleMenuMapper.batchRoleMenu(list);
        }
        return rows;
    }

    /**
     * 新增角色部门信息(数据权限)
     *
     * @param role 角色对象
     */
    public int insertRoleDept(FlyRole role)
    {
        int rows = 1;
        // 新增角色与部门（数据权限）管理
        List<FlyRoleDept> list = new ArrayList<FlyRoleDept>();
        for (Long deptId : role.getDeptIds())
        {
            FlyRoleDept rd = new FlyRoleDept();
            rd.setRoleId(role.getRoleId());
            rd.setDeptId(deptId);
            list.add(rd);
        }
        if (list.size() > 0)
        {
            rows = roleDeptMapper.batchRoleDept(list);
        }
        return rows;
    }
    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 通过角色ID删除角色
     *
     * @param roleId 角色ID
     * @return 结果
     */
    @Override
    public int deleteRoleById(Long roleId)
    {
        return roleMapper.deleteRoleById(roleId);
    }

    /**
     * 批量删除角色信息
     *
     * @param roleIds 需要删除的角色ID
     * @return 结果
     */
    public int deleteRoleByIds(Long[] roleIds)
    {
        for (Long roleId : roleIds)
        {
            checkRoleAllowed(new FlyRole(roleId));
            FlyRole role = selectRoleById(roleId);
            if (countUserRoleByRoleId(roleId) > 0)
            {
                throw new CustomException(String.format("%1$s已分配,不能删除", role.getRoleName()));
            }
        }
        return roleMapper.deleteRoleByIds(roleIds);
    }
    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改保存角色信息
     *
     * @param role 角色信息
     * @return 结果
     */
    @Override
    @Transactional
    public int updateRole(FlyRole role)
    {
        // 修改角色信息
        roleMapper.updateRole(role);
        // 删除角色与菜单关联
        roleMenuMapper.deleteRoleMenuByRoleId(role.getRoleId());
        return insertRoleMenu(role);
    }

    /**
     * 修改角色状态
     *
     * @param role 角色信息
     * @return 结果
     */
    public int updateRoleStatus(FlyRole role)
    {
        return roleMapper.updateRole(role);
    }

    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 校验角色名称是否唯一
     *
     * @param role 角色信息
     * @return 结果
     */
    @Override
    public String checkRoleNameUnique(FlyRole role)
    {
        Long roleId = StrUtils.isNull(role.getRoleId()) ? -1L : role.getRoleId();
        FlyRole info = roleMapper.checkRoleNameUnique(role.getRoleName());
        if (StrUtils.isNotNull(info) && info.getRoleId().longValue() != roleId.longValue())
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 校验角色权限是否唯一
     *
     * @param role 角色信息
     * @return 结果
     */
    @Override
    public String checkRoleKeyUnique(FlyRole role)
    {
        Long roleId = StrUtils.isNull(role.getRoleId()) ? -1L : role.getRoleId();
        FlyRole info = roleMapper.checkRoleKeyUnique(role.getRoleKey());
        if (StrUtils.isNotNull(info) && info.getRoleId().longValue() != roleId.longValue())
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 校验角色是否允许操作
     *
     * @param role 角色信息
     */
    public void checkRoleAllowed(FlyRole role)
    {
        if (StrUtils.isNotNull(role.getRoleId()) && role.isAdmin())
        {
            throw new CustomException("不允许操作超级管理员角色");
        }
    }

    /**
     * 通过角色ID查询角色使用数量
     *
     * @param roleId 角色ID
     * @return 结果
     */
    @Override
    public int countUserRoleByRoleId(Long roleId)
    {
        return userRoleMapper.countAdminRoleByRoleId(roleId);
    }

    /**
     * 根据用户ID查询权限
     *
     * @param userId 用户ID
     * @return 权限列表
     */
    @Override
    public Set<String> selectRolePermissionByUserId(Long userId)
    {
        List<FlyRole> perms = roleMapper.selectRolePermissionByAdminId(userId);
        Set<String> permsSet = new HashSet<>();
        for (FlyRole perm : perms)
        {
            if (StrUtils.isNotNull(perm))
            {
                permsSet.addAll(Arrays.asList(perm.getRoleKey().trim().split(",")));
            }
        }
        return permsSet;
    }

    /**
     * 查询所有角色
     *
     * @return 角色列表
     */
    public List<RoleQueryDTO> selectRoleAll()
    {
        List<FlyRole> roleList=roleMapper.selectRoleAll();
        List<RoleQueryDTO>  roleQueryDTOList = new ArrayList<>();
        roleList.forEach(role->{
            RoleQueryDTO entity=new RoleQueryDTO();
            entity.setRoleId(role.getRoleId());
            entity.setRoleName(role.getRoleName());
            entity.setStatus(role.getStatus());
            roleQueryDTOList.add(entity);
        });
        return roleQueryDTOList;
    }

    /**
     * 根据用户ID获取角色选择框列表
     *
     * @param adminId 用户ID
     * @return 选中角色ID列表
     */
    public List<Integer> selectRoleListByAdminId(Long adminId)
    {
        return roleMapper.selectRoleListByAdminId(adminId);
    }

    /**
     * 根据用户ID获取角色选择框列表
     *
     * @param adminId 用户ID
     * @return 选中角色ID列表
     */
    public List<FlyRole> selectRoleByAdminId(Long adminId)
    {
        return roleMapper.selectRoleByAdminId(adminId);
    }


    /**
     * 通过角色ID查询角色
     *
     * @param roleId 角色ID
     * @return 角色对象信息
     */
    public FlyRole selectRoleById(Long roleId)
    {
        return roleMapper.selectRoleById(roleId);
    }

    /**
     * 根据条件分页查询角色数据
     *
     * @param role 角色信息
     * @return 角色数据集合信息
     */
    @Override
    @DataScope(deptAlias = "d")
    public Pager<FlyRoleVo> selectRolePager(FlyRole role, Integer page, Integer pageSize, String sort, String order)
    {
        Pager<FlyRoleVo> pager=new Pager(page,pageSize);
        pager.setEntity(role);
        List<FlyRole> roleList=roleMapper.selectRolePager(pager);
        List<FlyRoleVo> volsit = new ArrayList<FlyRoleVo>();
        roleList.forEach(roles -> {
            FlyRoleVo roleVo = new FlyRoleVo();
            roleVo.setRoleId(roles.getRoleId());
            roleVo.setRoleName(roles.getRoleName());
            roleVo.setRoleKey(roles.getRoleKey());
            roleVo.setRoleSort(roles.getRoleSort());
            roleVo.setStatus(roles.getStatus());
            roleVo.setCreateTime(roles.getCreateTime().getTime());
            volsit.add(roleVo);
        });
        pager.setList(volsit);
        pager.setTotal(roleMapper.queryRoleTotal(pager));
        return pager;
    }
}
