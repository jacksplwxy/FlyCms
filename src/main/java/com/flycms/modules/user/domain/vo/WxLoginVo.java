package com.flycms.modules.user.domain.vo;

import lombok.Data;

@Data
public class WxLoginVo {
	private String code;
	private String wxCode;
	private String mobile;
	private String password;
	private String rawData;
	private String encryptedData;
	private String iv;
}
