package com.flycms.modules.user.service.impl;

import com.flycms.common.constant.Constants;
import com.flycms.common.constant.UserConstants;
import com.flycms.common.utils.*;
import com.flycms.common.utils.bean.BeanConvertor;
import com.flycms.common.utils.page.Pager;
import com.flycms.framework.config.properties.FlyMallProperties;
import com.flycms.modules.user.domain.dto.UserInfoDTO;
import com.flycms.modules.user.service.IUserService;
import com.flycms.modules.user.domain.User;
import com.flycms.modules.user.domain.UserAccount;
import com.flycms.modules.user.domain.dto.UserDTO;
import com.flycms.modules.user.mapper.UserMapper;
import com.flycms.modules.user.service.IUserAccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * 用户管理Service业务层处理
 * 
 * @author admin
 * @date 2020-05-24
 */
@Service
public class UserServicesImpl implements IUserService
{
    private static final Logger log = LoggerFactory.getLogger(UserServicesImpl.class);

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private IUserAccountService userAccountService;

    /////////////////////////////////
    ///////       增加       ////////
    /////////////////////////////////
    /**
     * 新增用户管理
     *
     * @param user 用户管理
     * @return 结果
     */
    @Override
    @Transactional
    public int insertUser(User user)
    {
        user.setId(SnowFlakeUtils.nextId());
        user.setCreateTime(DateUtils.getNowDate());
        user.setLastLoginTime(DateUtils.getNowDate());
        UserAccount userAccount=new UserAccount();
        //用户账户信息
        userAccount.setUserId(user.getId());
        userAccountService.insertUserAccount(userAccount);
        return userMapper.insertUser(user);
    }

    /////////////////////////////////
    ///////        刪除      ////////
    /////////////////////////////////
    /**
     * 批量删除用户管理
     *
     * @param ids 需要删除的用户管理ID
     * @return 结果
     */
    @Override
    public int deleteUserByIds(Long[] ids)
    {
        return userMapper.deleteUserByIds(ids);
    }

    /**
     * 删除用户管理信息
     *
     * @param id 用户管理ID
     * @return 结果
     */
    @Override
    public int deleteFlyUserById(Long id)
    {
        return userMapper.deleteUserById(id);
    }


    /////////////////////////////////
    ///////        修改      ////////
    /////////////////////////////////
    /**
     * 修改用户管理
     *
     * @param user 用户管理
     * @return 结果
     */
    @Override
    public int updateUser(User user)
    {
        user.setUpdateTime(DateUtils.getNowDate());
        return userMapper.updateUser(user);
    }

    /**
     * 修改用户头像
     *
     * @param user 用户信息
     * @param image  头像地址
     * @return
     */
    @Override
    public int updateUserAvatar(User user, BufferedImage image) {
        try {
            String savePath = FlyMallProperties.getAvatarPath() +"/";
            //文件保存目录URL
            String saveUrl  = Constants.RESOURCE_PREFIX;
            //创建文件夹
            SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
            String ymds=sdf.format(user.getCreateTime());
            savePath += "/"+ymds + "/"+user.getId() + "/";
            saveUrl += "/avatar/"+ymds + "/"+user.getId() + "/";
            File dirFile = new File(savePath);
            if (!dirFile.exists()) {
                dirFile.mkdirs();
            }
            String fileName = "avatar.jpg";
            File file = new File(savePath + fileName);
            if (!file.exists()) {
                file.createNewFile();
            }
            ImageIO.write(image, "PNG", file);
            //System.out.println(savePath+"----------------"+saveUrl + fileName);
            return userMapper.updateUserAvatar(user.getId(),saveUrl + fileName);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

    /**
     * 修改用户密码
     *
     * @param id 用户id
     * @param password  用户密码
     * @return
     */
    @Override
    public int updatePassword(Long id,String password){
        String newPassword= SecurityUtils.encryptPassword(password);
        return userMapper.updatePassword(id,newPassword);
    }
    /////////////////////////////////
    ///////        查詢      ////////
    /////////////////////////////////
    /**
     * 校验账号是否唯一
     *
     * @param user 用户
     * @return 结果
     */
    @Override
    public String checkUsernameUnique(User user)
    {
        int count = userMapper.checkUsernameUnique(user);
        if (count > 0)
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 校验手机号码是否唯一
     *
     * @param user 用户
     * @return 结果
     */
    @Override
    public String checkUserMobileUnique(User user)
    {
        int count = userMapper.checkUserMobileUnique(user);
        if (count > 0)
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 校验邮箱是否唯一
     *
     * @param user 用户
     * @return 结果
     */
    @Override
    public String checkUserEmailUnique(User user)
    {
        int count = userMapper.checkUserEmailUnique(user);
        if (count > 0)
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 校验短网址是否唯一
     *
     * @param shortUrl 用户
     * @return 结果
     */
    @Override
    public String checkUserShorturlUnique(String shortUrl)
    {
        int count = userMapper.checkUserShorturlUnique(shortUrl);
        if (count > 0)
        {
            return UserConstants.NOT_UNIQUE;
        }
        return UserConstants.UNIQUE;
    }

    /**
     * 查询用户短域名是否被占用
     *
     * @return
     */
    @Override
    public String shortUrl(){
        String[] aResult = ShortUrlUtils.shortUrl (null);
        String code=null;
        for ( int i = 0; i < aResult. length ; i++) {
            code=aResult[i];
            //查询问答短域名是否被占用
            if(userMapper.checkUserShorturlUnique(code) == 0){
                break;
            }
        }
        return code;
    }

    /**
     * 查询用户信息
     * 
     * @param id 用户管理ID
     * @return 用户管理
     */
    @Override
    @Cacheable(value = "user",key = "'user_'+#id",sync=true)
    public User findUserById(Long id)
    {
        return userMapper.findUserById(id);
    }

    /**
     * 按shortUrl查询用户信息
     *
     * @param shortUrl
     * @return
     */
    @Override
    @Cacheable(value = "user",key = "'user_'+#shortUrl",sync=true)
    public User findUserByShorturl(String shortUrl){
        return userMapper.findUserByShorturl(shortUrl);
    }
    /**
     * 查询用户管理
     *
     * @param id 用户管理ID
     * @return 用户管理
     */
    @Override
    @Cacheable(value = "user",key = "'userInfo_'+#id",sync=true)
    public UserInfoDTO findUserInfoById(Long id)
    {
        User user = userMapper.findUserById(id);
        return BeanConvertor.convertBean(user, UserInfoDTO.class);
    }

    /**
     * 按username查询用户管理详细信息
     *
     * @param username
     * @return
     */
    @Override
    public User findUserByUsername(String username)
    {
        return userMapper.findUserByUsername(username);
    }

    /**
     * 按手机号码查询用户管理详细信息
     *
     * @param mobile
     * @return
     */
    @Override
    public User findUserByMobile(String mobile)
    {
        return userMapper.findUserByMobile(mobile);
    }

    /**
     * 按邮箱查询用户管理详细信息
     *
     * @param email
     * @return
     */
    @Override
    public User findUserByEmail(String email)
    {
        return userMapper.findUserByEmail(email);
    }


    /**
     * 查询用户列表
     *
     * @param user 用户
     * @return 用户
     */
    @Override
    public Pager<UserDTO> selectUserPager(User user, Integer page, Integer pageSize, String sort, String order)
    {
        Pager<UserDTO> pager=new Pager(page,pageSize);
        //排序设置
        if (!StrUtils.isEmpty(sort)) {
            Boolean rank = "desc".equals(order) ? true : false;
            pager.addOrderProperty(sort, rank,true);
        }
        //使用limit进行查询翻页
        pager.addLimitProperty(true);
        pager.setEntity(user);

        List<User> userList=userMapper.selectUserPager(pager);
        List<UserDTO> dtolsit = new ArrayList<UserDTO>();
        userList.forEach(entity -> {
            UserDTO dto = new UserDTO();
            dto.setId(entity.getId());
            dto.setShortUrl(entity.getShortUrl());
            dto.setUsername(entity.getUsername());
            dto.setMobile(entity.getMobile());
            dto.setEmail(entity.getEmail());
            dto.setGender(entity.getGender());
            dto.setBirthday(entity.getBirthday());
            dto.setLastLoginTime(entity.getLastLoginTime());
            dto.setLastLoginIp(entity.getLastLoginIp());
            dto.setUserLevel(entity.getUserLevel());
            dto.setNickname(entity.getNickname());
            dto.setSignature(entity.getSignature());
            dto.setAbout(entity.getAbout());
            dto.setAvatar(entity.getAvatar());
            dto.setStatus(entity.getStatus());
            dtolsit.add(dto);
        });
        pager.setList(dtolsit);
        pager.setTotal(userMapper.queryUserTotal(pager));
        return pager;
    }

    /**
     * 查询需要导出的用户列表
     *
     * @param user 用户信息
     * @return 用户集合
     */
    @Override
    public List<UserDTO> exportUserList(User user) {
        return BeanConvertor.copyList(userMapper.exportUserList(user),UserDTO.class);
    }

}
