$(document).ready(function(){
    //响应式导航条效果
    $('.aw-top-nav .navbar-toggle').click(function() {
        if ($(this).parents('.aw-top-nav').find('.navbar-collapse').hasClass('active'))
        {
            $(this).parents('.aw-top-nav').find('.navbar-collapse').removeClass('active');
        }
        else
        {
            $(this).parents('.aw-top-nav').find('.navbar-collapse').addClass('active');
        }
    });

    $('.aw-topic-recommend-list #aw-more-recommend').click(function()
    {
        $('.aw-topic-list-mod .nav-tabs li:eq(2) a').click();
    })

    $('.operate .btn-group').click(function(){
        if ($(this).hasClass('open'))
        {
            $(this).removeClass('open');
        }
        else
        {
            $(this).addClass('open');
        }
    });

    $('.aw-nav-tabs a').click(function (e) {
        e.preventDefault()
        $(this).tab('show')
    })

    $(function(){
        var url = window.location.href;
        if(url!="" && url!=undefined){
            var match = url.match(/(?<=topic\/)\d+(?=.?)/);
            if(match){
                var id = match[0];
                $.get("/topic/update/view",{ id: id });
            }
        }
    });

    if ( $(".swiper-container").length > 0 ) {
        var mySwiper = new Swiper('.swiper-container',{
            pagination: '.pagination',
            autoplay : 5000,//可选选项，自动滑动
            loop:true,
            grabCursor: true,
            paginationClickable: true
        })
        $('.arrow-left').on('click', function(e){
            e.preventDefault()
            mySwiper.swipePrev()
        })
        $('.arrow-right').on('click', function(e){
            e.preventDefault()
            mySwiper.swipeNext()
        })
    }
    if ( $("#e").length > 0 ) {
        $('#e').rollNoInterval().left();
    }

    //用户关注话题
    $(".topic-follow").on("click", function(){
        var id = $(this).attr("data-topic-id");
        $.ajax({
            url: '/topic/user/follow',
            data: {'id': id},
            dataType: "json",
            type :  "POST",
            cache : false,
            async: false,
            error : function(i, g, h) {
                layer.msg('发送错误', {icon: 2});
            },
            success: function(data){
                if(data.code==200){
                    layer.msg(data.msg, {shade: 0.2,icon: 1});
                    window.location.reload();
                }else{
                    layer.msg(data.msg, {shade: 0.2,icon: 2});
                }
                window.location.reload();
            }
        });
    });

    //关注用户
    $(".follow-user").on("click", function(){
        var id = $(this).attr("data-user-id");
        $.ajax({
            url: '/people/user/follow',
            data: {'id': id},
            dataType: "json",
            type :  "POST",
            cache : false,
            async: false,
            error : function(i, g, h) {
                layer.msg('发送错误', {icon: 2});
            },
            success: function(data){
                if(data.code==3){
                    $(".follow-user").removeClass("active");
                    $(".follow-user").children("span").html("关注");
                    $(".follow-user").children("b").html(data.data.count);
                    layer.msg(data.msg, {shade: 0.2,time:1000,icon: 2});
                    return;
                }if(data.code==4){
                    $(".follow-user").addClass("active");
                    $(".follow-user").children("span").html("取消关注");
                    $(".follow-user").children("b").html(data.data.count);
                    layer.msg(data.msg, {shade: 0.2,time:1000,icon: 1});
                    return;
                }else{
                    layer.msg(data.msg, {shade: 0.2,time:1000,icon: 2});
                    return;
                }
            }
        });
    });

    //关注标签
    $(".follow-label").on("click", function(){
        var id = $(this).attr("data-label-id");
        $.ajax({
            url: '/t/user/follow',
            data: {'id': id},
            dataType: "json",
            type :  "POST",
            cache : false,
            async: false,
            error : function(i, g, h) {
                layer.msg('发送错误', {icon: 2});
            },
            success: function(data){
                if(data.code==3){
                    $(".follow-label").removeClass("active");
                    $(".follow-label").children("span").html("关注");
                    $(".follow-label").children("b").html(data.data.count);
                    layer.msg(data.msg, {shade: 0.2,time:1000,icon: 2});
                    return;
                }if(data.code==4){
                    $(".follow-label").addClass("active");
                    $(".follow-label").children("span").html("取消关注");
                    $(".follow-label").children("b").html(data.data.count);
                    layer.msg(data.msg, {shade: 0.2,time:1000,icon: 1});
                    return;
                }else{
                    layer.msg(data.msg, {shade: 0.2,time:1000,icon: 2});
                    return;
                }
            }
        });
    });

    //关注小组
    $(document).on('click', '.follow-group', function (){
        var groupId = $(this).attr("data-group-id");
        var text = $(this);
        $.ajax({
            url: '/group/user/follow',
            data: {"groupId":groupId},
            dataType: "json",
            type :  "post",
            cache : false,
            async: false,
            error : function(i, g, h) {
                layer.msg('发送错误', {icon: 2});
            },
            success: function(data){
                if(data.code==101){
                    layer.msg(data.msg, {icon: 2, time: 2000});
                    return false;
                }else if(data.code==501){
                    layer.msg(data.msg, {icon: 2, time: 2000});
                    return false;
                }else if(data.code==511){
                    text.removeClass('btn-success').addClass('alert-warning');
                    text.html("等待审核");
                    //弹出提示2秒后刷新页面
                    layer.msg(data.msg,{icon: 1, time: 2000});
                    return false;
                }else if(data.code==512){
                    text.removeClass('btn-success').addClass('alert-info');
                    text.html("加入小组");
                    //弹出提示2秒后刷新页面
                    layer.msg(data.msg,{icon: 1, time: 2000});
                    return false;
                }else if(data.code==520){
                    text.removeClass('btn-success').addClass('alert-info');
                    if(text.text() == "加入小组"){
                        text.html("退出小组");
                    }
                    //弹出提示2秒后刷新页面
                    layer.msg(data.msg,{icon: 1, time: 2000});
                    return false;
                }else if(data.code==521){
                    text.removeClass('alert-info').addClass('btn-success');
                    if(text.text() == "退出小组"){
                        text.html("加入小组");
                    }
                    //弹出提示2秒后刷新页面
                    layer.msg(data.msg,{icon: 2, time: 2000});
                    return false;
                }
            }
        });
    });

    $('.aw-tabs li').click(function() {
        $(this).addClass('active').siblings().removeClass('active');

        $('#focus .aw-user-center-follow-mod').eq($(this).index()).show().siblings().hide();
    });

    $('#fly-copyUrl').on('click',function(){
        console.log("11111111")
        copyUrl($(this));
    });
    function copyUrl (obj){
        if($('#urlText').length == 0){
            console.log("22221111")
            // 创建input
            obj.after('<input id="urlText" type="hidden" value=' + window.location.href + '>');
        }
        $('#urlText').select(); //选择对象
        document.execCommand("Copy"); // 执行浏览器复制命令
        layer.msg("复制成功!");
    }
});
