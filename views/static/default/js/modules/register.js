$(document).ready(function(){
    $(".aw-register-pwd").prev(".oldPwdTip").click(function(){
        $(".aw-register-pwd").focus();
    });
    $(".aw-register-pwd").focus(function(){
        $(".oldPwdTip").hide();
    });
    $(".aw-register-pwd").blur(function(){
        if(!$(this).val())
            $(".oldPwdTip").show();
    });
    var mobile;
    $.get('/about/agreement', function (result) { $('#register_agreement').html(result.data.content); }, 'json');
    $('.aw-agreement-btn').click(function()
    {
        if ($('.aw-register-agreement').is(':visible')) {
            $('.aw-register-agreement').hide();
        } else {
            $('.aw-register-agreement').show();
        }
    });

    $('.more-information-btn').click(function() {
        $('.more-information').fadeIn();
        $(this).parent().hide();
    });

    verify_register_form('#register_form');

    /* 注册页面验证 */
    function verify_register_form(element) {
        $(element).find('[type=text], [type=password]').on({
            focus : function() {
                if (typeof $(this).attr('tips') != 'undefined' && $(this).attr('tips') != '')
                {
                    $(this).parent().append('<b class="aw-reg-tips">' + $(this).attr('tips') + '</b>');
                }
            },
            blur : function() {
                if ($(this).attr('tips') != '')
                {
                    switch ($(this).attr('name'))
                    {
                        case 'username' :
                            var _this = $(this);
                            var min_length=2;
                            var max_length=16;
                            $(this).parent().find('.aw-reg-tips').detach();
                            if (strlen($(this).val()) >= 0 && strlen($(this).val())< min_length)
                            {
                                $(this).parent().find('.aw-reg-tips').detach();
                                $(this).parent().append('<b class="aw-reg-tips aw-reg-err"><i class="aw-icon i-err"></i>' + $(this).attr('errortips') + '</b>');
                                return;
                            }
                            if (strlen($(this).val()) > max_length)
                            {
                                $(this).parent().find('.aw-reg-tips').detach();
                                $(this).parent().append('<b class="aw-reg-tips aw-reg-err"><i class="aw-icon i-err"></i>' + $(this).attr('errortips') + '</b>');
                                return;
                            }
                            else {
                                $.post( '/account/ajax/check_username/',{
                                    username: $(this).val()
                                }, function (result) {
                                    if (result.code==200) {
                                        _this.parent().find('.aw-reg-tips').detach();
                                        _this.parent().append('<b class="aw-reg-tips aw-reg-right"><i class="aw-icon i-followed"></i></b>');

                                    } else {
                                        _this.parent().find('.aw-reg-tips').detach();
                                        _this.parent().append('<b class="aw-reg-tips aw-reg-err"><i class="aw-icon i-err"></i>' + result.msg + '</b>');
                                    }
                                }, 'json');
                            }
                            return;
                        case 'email' :
                            $(this).parent().find('.aw-reg-tips').detach();
                            var emailreg = /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/;
                            if (!emailreg.test($(this).val())) {
                                $(this).parent().find('.aw-reg-tips').detach();
                                $(this).parent().append('<b class="aw-reg-tips aw-reg-err"><i class="aw-icon i-err"></i>' + $(this).attr('errortips') + '</b>');
                                return;
                            }  else {
                                $.post( '/account/ajax/check_email/',
                                    {
                                        useremail: $(this).val()
                                    }, function (result) {
                                        if (result.code==200) {
                                            _this.parent().find('.aw-reg-tips').detach();
                                            _this.parent().append('<b class="aw-reg-tips aw-reg-right"><i class="aw-icon i-followed"></i></b>');

                                        } else {
                                            _this.parent().find('.aw-reg-tips').detach();
                                            _this.parent().append('<b class="aw-reg-tips aw-reg-err"><i class="aw-icon i-err"></i>' + result.msg + '</b>');
                                        }
                                    }, 'json');
                            }
                            return;

                        case 'password' :
                            $(this).parent().find('.aw-reg-tips').detach();
                            if ($(this).val().length >= 0 && $(this).val().length < 6)
                            {
                                $(this).parent().find('.aw-reg-tips').detach();
                                $(this).parent().append('<b class="aw-reg-tips aw-reg-err"><i class="aw-icon i-err"></i>' + $(this).attr('errortips') + '</b>');
                                return;
                            }
                            if(!/^(?![a-zA-z]+$)(?!\d+$)(?![!@#$%^&*]+$)[a-zA-Z\d!@#$%^&*]{6,32}$/.test($(this).val())){
                                $(this).parent().find('.aw-reg-tips').detach();
                                $(this).parent().append('<b class="aw-reg-tips aw-reg-err"><i class="aw-icon i-err"></i>' + $(this).attr('errortips') + '</b>');
                                return;
                            }
                            else
                            {
                                $(this).parent().find('.aw-reg-tips').detach();
                                $(this).parent().append('<b class="aw-reg-tips aw-reg-right"><i class="aw-icon i-followed"></i></b>');
                            }
                            return;

                        case 'mobile':
                            $(this).parent().find('.aw-reg-tips').detach();
                            var phonereg = /^1[345789]\d{9}$/;
                            var _this = $(this);
                            if (!phonereg.test($(this).val()))
                            {
                                $(this).parent().find('.aw-reg-tips').detach();
                                $(this).parent().append('<b class="aw-reg-tips aw-reg-err"><i class="aw-icon i-err"></i>' + $(this).attr('errortips') + '</b>');
                                return;
                            } else {
                                $.post( '/account/ajax/check_mobile/', {
                                    mobile: $(this).val()
                                }, function (result) {
                                    if (result.code==200) {
                                        _this.parent().find('.aw-reg-tips').detach();
                                        _this.parent().append('<b class="aw-reg-tips aw-reg-right"><i class="aw-icon i-followed"></i></b>');

                                    } else {
                                        _this.parent().find('.aw-reg-tips').detach();
                                        _this.parent().append('<b class="aw-reg-tips aw-reg-err"><i class="aw-icon i-err"></i>' + result.msg + '</b>');
                                    }
                                }, 'json');
                            }
                            return;
                        case 'smscode':
                            $(this).parent().find('.aw-reg-tips').detach();
                            if (!$(this).val())
                            {
                                $(this).parent().find('.aw-reg-tips').detach();
                                $(this).parent().append('<b class="aw-reg-tips aw-reg-err"><i class="aw-icon i-err"></i>' + $(this).attr('errortips') + '</b>');
                                return;
                            }
                            else
                            {
                                $(this).parent().find('.aw-reg-tips').detach();
                            }
                            return;
                    }
                }
            }
        });
    }

    //判断字符长度
    function strlen(str){
        var len = 0;
        for (var i=0; i<str.length; i++) {
            var c = str.charCodeAt(i);
            //单字节加1
            if ((c >= 0x0001 && c <= 0x007e) || (0xff60<=c && c<=0xff9f)) {
                len++;
            }
            else {
                len+=2;
            }
        }
        return len;
    }
    if ($("#mpanel2").length > 0 ) {
        // 初始化验证码  弹出式
        $('#mpanel2').slideVerify({
            baseUrl: window.location.protocol+"//"+window.location.host,  //服务器请求地址;
            mode:'pop',     //展示模式
            containerId:'j-captcha-sense',//pop模式 必填 被点击之后出现行为验证码的元素id
            imgSize : {       //图片的大小对象,有默认值{ width: '310px',height: '155px'},可省略
                width: '380px',
                height: '180px',
            },
            barSize:{          //下方滑块的大小对象,有默认值{ width: '310px',height: '50px'},可省略
                width: '380px',
                height: '40px',
            },
            beforeCheck:function(){  //检验参数合法性的函数  mode ="pop"有效
                var flag = true;
                //实现: 参数合法性的判断逻辑, 返回一个boolean值
                return flag
            },
            ready : function() {},  //加载完毕的回调
            success : function(params) { //成功的回调
                $("#captcha").val(params.captchaVerification);
                $(".yidun_intellisense").addClass("yidun_intellisense--success");
                $(".yidun_intelli-tips").hide();
            },
            error : function() {}        //失败的回调
        });
    }


    $("#Smsbtn").click(function (e) {
        var username = $('#username').val();
        var phonereg = /^1[3456789]\d{9}$/;
        var mobile = $('input[name="mobile"]').val();
        var captcha = $('#captcha').val();
        if(username=='' ){
            layer.msg('请输入用户名', {shade: 0.2,time:3000,icon: 2});
            return false;
        }
        if (!username.match(/^[a-zA-Z\u4E00-\u9FA5]{1}[\u4E00-\u9FA5a-zA-Z0-9_]{1,16}$/)) {
            layer.msg('用户名格式不正确', {shade: 0.2,time:1000,icon: 2});
            return false;
        }
        if(mobile=='' ){
            layer.msg('请输入手机号', {shade: 0.2,time:1000,icon: 2});
            return false;
        }
        if(!phonereg.test(mobile)){
            layer.msg('手机号格式不正确', {shade: 0.2,time:1000,icon: 2});
            return false;
        }
        if($('#Smsbtn').text()!="获取验证码"){
            return false;
        }
        if (captcha==null || captcha =="") {
            layer.msg('请点击安全安验证码', {shade: 0.2,time:1000,icon: 2});
            return false;
        } else{
            $.post( '/account/ajax/check_username/', {
                username: username
            }, function (result) {
                if (result.code==200) {
                    $('#username').parent().find('.aw-reg-tips').detach();
                    $('#username').parent().append('<b class="aw-reg-tips aw-reg-right"><i class="aw-icon i-followed"></i></b>');
                    var count = 60;
                    var countdown = setInterval(CountDown, 1000);
                    function CountDown() {
                        $('#Smsbtn').prop('disabled', true);
                        $("#verifytime").val(count);
                        $("#Smsbtn").text(count + " 秒后");
                        if (count == 0) {
                            $("#verify_code").html("发送验证码");
                            $('#Smsbtn').prop('disabled', false);
                            $('#Smsbtn').text('获取验证码');
                            clearInterval(countdown);
                        }
                        count--;
                    }
                    $.post( '/register/send/phone',{
                        mobile:mobile,
                        captcha:captcha,
                        type:'regist'
                    }, function (result){
                        if (result.code == 200){
                            layer.msg(result.msg, {shade: 0.2,icon: 1});
                            return false;
                        }else{
                            layer.msg('手机号已经注册', {shade: 0.2,icon: 2});
                            $("#captcha").val("");
                            $(".yidun_intellisense").removeClass("yidun_intellisense--success");
                            $(".yidun_intelli-tips").show();
                            return false;
                        }
                    }, 'json');
                } else {
                    layer.msg(result.msg, {shade: 0.2,icon: 2});
                    $('#username').parent().find('.aw-reg-tips').detach();
                    $('#username').parent().append('<b class="aw-reg-tips aw-reg-err"><i class="aw-icon i-err"></i>' + result.msg + '</b>');
                }
            }, 'json');
        }
    });

    $("#agreement").change(function() {
        var state=$(this).prop('checked');
        if(state){
            $("#zhuce").removeAttr("disabled");
            alert("同意");
        }else{
            $("#zhuce").attr({"disabled":"disabled"});
            alert("不同意");
        }
    });

    $('#zhuce').click(function(){
        var username=$("#username").val();
        var phonereg = /^1[3456789]\d{9}$/;
        var mobile = $('input[name="mobile"]').val();
        var code=$('input[name="smscode"]').val();
        var password=$("#aw-register-pwd").val();
        var invite=$("#invite").val();
        var signature=$("#signature").val();
        if (!username.match(/^[a-zA-Z\u4E00-\u9FA5]{1}[\u4E00-\u9FA5a-zA-Z0-9_]{1,16}$/)) {
            $("#username").parent().find('.aw-reg-tips').detach();
            $("#username").parent().append('<b class="aw-reg-tips aw-reg-err"><i class="aw-icon i-err"></i>' + $("#username").attr('errortips') + '</b>');
            return;
        } else {
            $.post( '/account/ajax/check_username/',{
                username: $("#username").val()
            }, function (result) {
                if (result.code==200) {
                    $("#username").parent().find('.aw-reg-tips').detach();
                    $("#username").parent().append('<b class="aw-reg-tips aw-reg-right"><i class="aw-icon i-followed"></i></b>');

                } else {
                    $("#username").parent().find('.aw-reg-tips').detach();
                    $("#username").parent().append('<b class="aw-reg-tips aw-reg-err"><i class="aw-icon i-err"></i>' + result.msg + '</b>');
                    return false;
                }
            }, 'json');
        }

        if (!phonereg.test(mobile)) {
            $("#phone").parent().find('.aw-reg-tips').detach();
            $("#phone").parent().append('<b class="aw-reg-tips aw-reg-err"><i class="aw-icon i-err"></i>' + $("#phone").attr('errortips') + '</b>');
            return;
        } else {
            $.post( '/account/ajax/check_mobile/', {
                mobile: $("#phone").val()
            }, function (result) {
                if (result.code==200) {
                    $("#phone").parent().find('.aw-reg-tips').detach();
                    $("#phone").parent().append('<b class="aw-reg-tips aw-reg-right"><i class="aw-icon i-followed"></i></b>');

                } else {
                    $("#phone").parent().find('.aw-reg-tips').detach();
                    $("#phone").parent().append('<b class="aw-reg-tips aw-reg-err"><i class="aw-icon i-err"></i>' + result.msg + '</b>');
                    return false;
                }
            }, 'json');
        }

        if(code.length>0){
            if(!/^([0-9a-zA-Z]{4})|([0-9a-zA-Z]{6})$/.test(code)){
                $("#smscode").parent().find('.aw-reg-tips').detach();
                $("#smscode").parent().append('<b class="aw-reg-tips aw-reg-err"><i class="aw-icon i-err"></i>' + $("#smscode").attr('errortips') + '</b>');
                return false;
            }else{
                $("#smscode").parent().find('.aw-reg-tips').detach();
            }
        }else{
            $("#smscode").parent().find('.aw-reg-tips').detach();
            $("#smscode").parent().append('<b class="aw-reg-tips aw-reg-err"><i class="aw-icon i-err"></i>' +$("#smscode").attr('errortips') + '</b>');
            return false;
        }

        if(password.length>=6){
            //if(!/^[0-9a-zA-Z_~!@#$%^&*()_+]{6,20}$/.test(password)){
            if(!/^(?![a-zA-z]+$)(?!\d+$)(?![!@#$%^&*]+$)[a-zA-Z\d!@#$%^&*]{6,32}$/.test(password)){
                $("#aw-register-pwd").parent().find('.aw-reg-tips').detach();
                $("#aw-register-pwd").parent().append('<b class="aw-reg-tips aw-reg-err"><i class="aw-icon i-err"></i>' + $("#aw-register-pwd").attr('errortips') + '</b>');
                return false;
            }else{
                $("#aw-register-pwd").parent().find('.aw-reg-tips').detach();
            }
        }else{
            $("#aw-register-pwd").parent().find('.aw-reg-tips').detach();
            $("#aw-register-pwd").parent().append('<b class="aw-reg-tips aw-reg-err"><i class="aw-icon i-err"></i>' + $("#aw-register-pwd").attr('errortips') + '</b>');
            return false;
        }

        jQuery.ajax({
            url: "/register/add/phone",
            data: {
                "username":username,
                "mobile":mobile,
                "code":code,
                "password":password,
                "invite":invite,
                "signature":signature
            },
            dataType: "json",
            type :  "POST",
            cache : false,
            async: false,
            error : function(i, g, h) {
                layer.msg('发送错误', {icon: 2});
            },
            success : function(data) {
                if(data.code==200){
                    window.location.href = data.data;
                    return false;
                }else{
                    layer.msg(data.msg, {icon: 2});
                    return false;
                }
            }
        });
        return false;
    });

    //手机号找回密码
    $("#mobileNext").click(function (e) {
        var phonereg = /^1[3456789]\d{9}$/;
        var mobile = $('input[name="mobile"]').val();
        var captcha = $('#captcha').val();
        if(mobile=='' ){
            layer.msg('请输入手机号', {shade: 0.2,time:1000,icon: 2});
            return false;
        }
        if(!phonereg.test(mobile)){
            layer.msg('手机号格式不正确', {shade: 0.2,time:1000,icon: 2});
            return false;
        }
        if($('#mobileNext').text()!="下一步"){
            return false;
        }
        if (captcha==null || captcha =="") {
            layer.msg('请点击安全安验证码', {shade: 0.2,time:1000,icon: 2});
            return false;
        } else{
            $.post( '/account/ajax/check_mobile/', {
                mobile: mobile
            }, function (result) {
                if (result.code==102) {
                    var count = 60;
                    var countdown = setInterval(CountDown, 1000);
                    function CountDown() {
                        $('#mobileNext').prop('disabled', true);
                        $("#verifytime").val(count);
                        $("#mobileNext").text(count + " 秒后");
                        if (count == 0) {
                            $("#verify_code").html("发送验证码");
                            $('#mobileNext').prop('disabled', false);
                            $('#mobileNext').text('下一步');
                            clearInterval(countdown);
                        }
                        count--;
                    }
                    $.post( '/register/send/phone',{
                        mobile:mobile,
                        captcha:captcha,
                        type:'find_password'
                    }, function (result){
                        if (result.code == 200){
                            layer.msg(result.msg, {shade: 0.2,icon: 1});
                            window.location.href = "/account/find_password/type-mobile/modify?mobile=" + mobile ;
                            return false;
                        }else{
                            layer.msg(result.msg, {shade: 0.2,icon: 2});
                            $("#captcha").val("");
                            $(".yidun_intellisense").removeClass("yidun_intellisense--success");
                            $(".yidun_intelli-tips").show();
                            return false;
                        }
                    }, 'json');
                } else {
                    layer.msg("用户不存在", {shade: 0.2,icon: 2});
                }
            }, 'json');
        }
    });

    $('#find_password').blur(function(){
        var password=$("#find_password").val();
        if(!/^(?![a-zA-z]+$)(?!\d+$)(?![!@#$%^&*]+$)[a-zA-Z\d!@#$%^&*]{6,32}$/.test(password)){
            $("#find_password").parent().find('.aw-reg-tips').detach();
            $("#find_password").parent().append('<b class="aw-reg-tips aw-reg-err"><i class="aw-icon i-err"></i>' + $("#find_password").attr('errortips') + '</b>');
            return false;
        }else{
            $("#find_password").parent().find('.aw-reg-tips').detach();
        }
    });

    $('#find_re_password').blur(function(){
        var password=$("#find_password").val();
        var repassword=$("#find_re_password").val();
        if(password!=repassword){
            $("#find_re_password").parent().find('.aw-reg-tips').detach();
            $("#find_re_password").parent().append('<b class="aw-reg-tips aw-reg-err"><i class="aw-icon i-err"></i>' + $("#find_re_password").attr('errortips') + '</b>');
            return false;
        }else{
            $("#find_re_password").parent().find('.aw-reg-tips').detach();
        }
    });

    $("#resetMobilePassword").click(function (e) {
        var mobile = $("#mobile").val();
        var password=$("#find_password").val();
        var repassword=$("#find_re_password").val();
        var code=$('input[name="smscode"]').val();
        jQuery.ajax({
            url: "/account/find_password/type-mobile/modify",
            data: {
                "mobile":mobile,
                "password":password,
                "repassword":repassword,
                "code":code
            },
            dataType: "json",
            type :  "POST",
            cache : false,
            async: false,
            error : function(i, g, h) {
                layer.msg('发送错误', {icon: 2});
            },
            success : function(data) {
                if(data.code==200){
                    window.location.href = "/user/login";
                    return false;
                }else{
                    layer.msg(data.msg, {icon: 2});
                    return false;
                }
            }
        });
    });
});